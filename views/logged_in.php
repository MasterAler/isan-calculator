<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Calculations page</title>
    <link rel="stylesheet" type="text/css"  href="/css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-form.js"></script>

    <link rel="stylesheet" type="text/css"  href="/css/style.css"/>
    <script>
        $(function(){
            $("a, button").button();
            $("a.data-ref").button( "destroy" );
            $("#dataForm").form();

            var $err = $("#error");
            var $info = $("#info");

            $("#dataForm").submit(function(e) {
                $("#send_calc").button( "option", "disabled", true);
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                // var formURL = "classes/zmq_send.php";
                $.ajax(
                    {
                        url : formURL,
                        type: "POST",
                        data : postData,
                        success:function(data, textStatus, jqXHR)
                        {
//                        var result = JSON.parse(data);
                            if (0 == data)
                            {
                                $info.html("Your calculations request was added to the queue");
                                $info.show();
                                setTimeout(function() {
                                    $info.fadeOut("slow");
                                    $info.html('');
                                    window.location.reload(true); 
									//window.location = window.location.href; // no stupid confirmations on reload
                                }, 3000);
                            }
                            else
                            {
                                $err.html("Error occured, try again later");
                                $err.show();
                                setTimeout(function() {$err.fadeOut("slow"); $err.html(''); }, 3000);
                            }
                            //alert("success " /*+ data*/);
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            $err.html("Error" + errorThrown + " occured, try again later");
                            $err.show();
                            setTimeout(function() {$err.fadeOut("slow"); $err.html(''); }, 3000);
                        }
                    });
                e.preventDefault(); //STOP default action
                e.unbind(); //unbind. to stop multiple form submit.
                
                return false;
            });

            if ($err.html())
            {
                $err.show();
                setTimeout(function() {$err.fadeOut("slow") }, 3000);
            }

            if ($info.html())
            {
                $info.show();
                setTimeout(function() {$info.fadeOut("slow") }, 3000);
            }
        });
    </script>
    <?php if (isset($_GET["action"]) &&("prev_calc" == $_GET["action"])) { ?>
        <script>
            $(function() {
                var url = '<?php echo SITE_URL . "classes/remove_archive.php" ?>';

                $("#container").find("a.prevremover").click(function(){
                    $.post(url,
                        {
                            id: $(this).data("id"),
                            user_name: '<?php echo $_SESSION["user_name"]; ?>'
                        },
                        function(result) {
                            // alert(result);
                            location.reload();
                        }
                    );
                });
            });
        </script>
    <?php  } ?>
    <?php if (isset($_GET["action"]) &&("config" == $_GET["action"])) { ?>
        <script>
            $(function(){
                var url = '<?php echo SITE_URL . "classes/remove_config.php" ?>';

                $("#container").find("a.cfremover").click(function(){
                    $.post(url,
                        {
                            id: $(this).data("id")
                        },
                        function(result) {
                           // alert(result);
                            location.reload();
                        }
                    );
                });

                var launcher = '<?php echo SITE_URL . "classes/converter_launcher.php" ?>';
                $("#reloader").click(function(){
                    $.post(launcher,
                        {
                            cmd: 42
                        },
                        function(result) {
                            var $info = $("#info");
                            $info.html(result + '\n converted');
                            $info.show();
                            setTimeout(function() {$info.fadeOut("slow"); $info.html(''); location.reload(); }, 3000);
                            // alert(result + '\n converted');
                        }
                    );
                });
				
            });
        </script>
    <?php }    ?>
    <?php if (isset($_GET["action"]) &&("users" == $_GET["action"])) { ?>
        <script>
            $(function(){
                var url = '<?php echo SITE_URL . "classes/remove_user.php" ?>';

                $("#users").find("a.remover").click(function(){
                    $.post(url,
                        {
                            id: $(this).data("id"),
                            remove_user: true
                        },
                        function(result) {
                            // alert(result);
                            location.reload();
                        }
                    );
                });
            });
        </script>
        <script>
            $(function() {

                var name = $( "#username" ),
                    email = $( "#email" ),
                    password = $( "#password" ),
                    allFields = $( [] ).add( name ).add( email ).add( password ),
                    tips = $( ".validateTips" );

                function updateTips( t ) {
                    tips
                        .text( t )
                        .addClass( "ui-state-highlight" );
                    setTimeout(function() {
                        tips.removeClass( "ui-state-highlight", 1500 );
                    }, 500 );
                }

                function checkLength( o, n, min, max ) {
                    if ( o.val().length > max || o.val().length < min ) {
                        o.addClass( "ui-state-error" );
                        updateTips( "Length of " + n + " must be between " +
                        min + " and " + max + "." );
                        return false;
                    } else {
                        return true;
                    }
                }

                function checkRegexp( o, regexp, n ) {
                    if ( !( regexp.test( o.val() ) ) ) {
                        o.addClass( "ui-state-error" );
                        updateTips( n );
                        return false;
                    } else {
                        return true;
                    }
                }

                function addUser()
                {
                    var url = '<?php echo SITE_URL . "classes/add_user.php" ?>';

                    $.post(url, {
                        'user_name': name.val(),
                        'user_password_new': password.val(),
                        'user_password_repeat': password.val(),
                        'user_email': email.val()
                    }, function(ans) {
                        var $err = $("#error");
                        var $info = $("#info");
                        var target = ans == "200" ? $info : $err;
                        var msg = ans == "200" ? "account has been created successfully" : ans;
                        target.html(msg);
                        target.show();
                        setTimeout(function() {target.fadeOut("slow"); location.reload(); }, 3000);
                    }).fail(function(data) {
                        console.log(data.error);
                        alert( "error" );
                    })
                }

                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    height: 380,
                    width: 350,
                    modal: true,
                    buttons: {
                        "Create user": function() {
                            var bValid = true;
                            allFields.removeClass( "ui-state-error" );

                            bValid = bValid && checkLength( name, "username", 3, 16 );
                            bValid = bValid && checkLength( email, "email", 6, 80 );
                            bValid = bValid && checkLength( password, "password", 5, 16 );

                            bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
                            // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                            bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
                            bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );

                            if ( bValid ) {
                                addUser();
                                $( this ).dialog( "close" );
                            }
                        },
                        "Cancel": function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                        allFields.val( "" ).removeClass( "ui-state-error" );
                    }
                });

                $( "#create-user" )
                    .button()
                    .click(function() {
                        $( "#dialog-form" ).dialog( "open" );
                    });
            });
        </script>
    <?php } ?>
    <?php if (!isset($_GET["action"])) {
        require_once("classes/ConfigLoader.php");
        $cfLoader = new ConfigLoader();
        ?>
        <script>
            $(function(){
                $("#container").height(700);
                /*   $("#result").css("height","calc(890px - " + $("#dataForm").height() + "px)")*/

                function generateHtml(data)
                {
                    function isNumber(n) {
                        return !isNaN(parseFloat(n)) && isFinite(n);
                    }

                    var template = '<p><label for="#name">#full_name</label><input type="#type" id="#name" name="#name" value="#data" min="#min" max="#max" step="#step" /> </p>';

                    var name_list = [];
                    var result = '';
                    for (var i=0; i<data.length; ++i)
                    {
                        var param = data[i];
                        if (name_list.indexOf(param.name) > -1)
                            continue;

                        name_list.push(param.name);
                        var full_name = param.name + (param.units  == null ? '' : ' (' + param.units + ')');    

                        if (param.type == 'num')
                        {
                            result += template.replace(/#name/gi, param.name)
                                .replace("#type", "number")
                                .replace("#full_name", full_name)
                                .replace("#min", param.min)
                                .replace("#max", param.max)
                                .replace("#step", param.step)
                                .replace("#data", param.value);
                            result += "\n";
                        }
                        else
                        {
                            result += template.replace(/#name/gi, key).replace("#type", "text").replace("#data", data[key]);
                            result += "\n";
                        }
                    }

                    return result;
                }

                var formData = JSON.parse('<?php echo $cfLoader->getConfigsJson(); ?>');
                $chooser = $("#config_chooser");
                $chooser.change(function(){
                    var current = $(this).val();
                    $("#params").html(generateHtml(formData[current]));
                    $("#config_filename").val(current);
                });

                $("#params").html(generateHtml(formData[$chooser.val()]));
                $("#config_filename").val($chooser.val());
				
				var url2 = '<?php echo SITE_URL . "classes/postprocess_launcher.php" ?>';
				$("#afterlaunch").click(function(){
                    $.post(url2,
                        {
                            script: $("#process-chooser").val()
                        },
                        function(result) {
                           // alert(result);
						   $("#p-out").val(result);
                        }
                    );
                });
				
            });
        </script>
        <script type="text/javascript" src="/js/calc_form_handler.js"></script>
    <?php } ?>
</head>
<body>
<h1 class="title">Welcome, <?php echo $_SESSION['user_name']; ?> !</h1>
<a href="?" class="nav-link">Calculations</a>
<a href="?action=prev_calc" class="nav-link">Previous calculations</a>
<a href="?action=change_pwd" class="nav-link">Change password</a>
<a href="index.php?logout" class="nav-link-right">Logout</a>

<?php
require_once("classes/Login.php");
if (Login::ADMIN_USERNAME == $_SESSION['user_name']) {
    ?>
    <a href="?action=users" class="nav-link-right">Users management</a>
    <a href="?action=config" class="nav-link-right">Config management</a>
<?php } ?>
<div id="container">

    <?php
	require_once("classes/ConfigLoader.php");
	
    $errMsg = "";
    $infoMsg = "";

    if(!isset($_GET["action"])) {
        ?>
        <!-- Форма -->
        <h1>Parameters input form</h1>


        <p>
            <label for="config_chooser">choose configuration</label>
            <select name="opts" id="config_chooser">
                <?php
                foreach(array_keys($cfLoader->getConfigs()) as $conf)
                    echo '<option value="' . $conf . '">' . $conf . '</option>';
                ?>
            </select>
        </p>
        <hr/>

        <form action="classes/zmq_send.php" method="post" id="dataForm" style="min-height: 400px;">
            <div id="params"></div>
            <hr/><!-- <div style="float: right;"><button type="submit">Get json</button></div> -->
            <?php if (!$cfLoader->isWaiting()) { ?>
                <div style="float: right;"><button id="send_calc" type="submit">Send calculations</button></div>
            <?php } else { ?>
                <div class="status">Previous calculation is not finished, wait plz</div>
            <?php } ?>
            <input type="hidden" id="config_filename" name="filename"/>
            <input type="hidden" id="current_user" name="user" value="<?=$_SESSION["user_name"]?>"/>
        </form>
		
		Postprocessing:
		<div id="post-process">
			<p>
				<div><label for="after-chooser">Options:</label>
				<select name="after-chooser" id="process-chooser">
                <?php
                foreach($cfLoader->getPostProcessScripts() as $script)
                    echo '<option value="' . $script . '">' . $script . '</option>';
                ?>
            </select>
				</div>
				<button id="afterlaunch">Process</button>
			</p>
			
			<label for="output">Stdout:</label>
			<textarea name="output" id="p-out" textarea></textarea>
		</div>

    <?php
    } else if ("change_pwd" == $_GET["action"]) {
        require_once("classes/UserManagement.php");

        $userManager = new UserManagement();

        if (isset($userManager)) {
            if ($userManager->errors) {
                foreach ($userManager->errors as $error) {
                    $errMsg .= $error . "<br/>";
                }
            }
            if ($userManager->messages) {
                foreach ($userManager->messages as $message) {
                    $infoMsg .= $message . "<br/>";
                }
            }
        }
        ?>
        <form action="?action=change_pwd" method="post" id="pwdForm">
            <label for="old_password">Old password:</label><br/>
            <input name="old_password" type="password" pattern=".{6,}" /><br/>
            <label for="new_password">New password:</label><br/>
            <input name="new_password" type="password" pattern=".{6,}" /><br/>
            <label for="new_password_repeat">Confirm password:</label><br/>
            <input name="new_password_repeat" type="password" pattern=".{6,}" /><br/>
            <input type="submit" name="change_password" value="Change password" />
        </form>
    <?php
    } else if ("users" ==  $_GET["action"]) {
        require_once("classes/UserManagement.php");

        $userManager = new UserManagement();

        if (isset($userManager)) {
            if ($userManager->errors) {
                foreach ($userManager->errors as $error) {
                    $errMsg .= $error . "<br/>";
                }
            }
            if ($userManager->messages) {
                foreach ($userManager->messages as $message) {
                    $infoMsg .= $message . "<br/>";
                }
            }
        }

//     $users = $userManager->userList();
        $pager = $userManager->userPaginator();
        $pager->php_self = $pager->php_self . "?action=users";
        ?>
        <table id="users" width="100%" border="1" cellspacing="1" cellpadding="1" class="ui-widget ui-widget-content">
            <tr class="ui-widget-header">
                <th>Login</th><th>Email</th><th>Action</th>
            </tr>
            <?php //foreach($users as $user) { ?>
            <?php
            $rs = $pager->paginate();
            if(!$rs) die(mysql_error());
            while($user = mysql_fetch_assoc($rs)){
                ?>
                <tr>
                    <td><?php echo $user["user_name"] ?></td>
                    <td><?php echo $user["user_email"] ?></td>
                    <td><a href="#" class="remover" data-id="<?php echo $user["user_id"] ?>">Delete</a></td>
                </tr>
            <?php } ?>
        </table>
        <table id="pagination">
            <tr>
                <td><?php echo $pager->renderFullNav();?></td>
            <tr>
        </table>
        <button id="create-user">Add user</button>

        <div id="dialog-form" title="Create new user">
            <p class="validateTips">Complete all fields</p>

            <form id="regForm">
                <fieldset>
                    <label for="username">Login</label>
                    <input type="text" name="username" id="username" class="text ui-widget-content ui-corner-all">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all">
                </fieldset>
            </form>
        </div>

    <?php } else if ("prev_calc" ==  $_GET["action"]) { ?>
        <h2>Archive calculations:</h2>

        <?php
        require_once("classes/CalcHistory.php");

        $history = new CalcHistory();
        $items = $history->fetchCalcHistory();
        foreach($items as $item){
            echo '<div class="calcs"><p><a class="data-ref" href=' . $history->archivePath() . $item . ' target="_blank">'
                . $item . '</a>';
            echo '<a href="#" style="float: right;" class="prevremover" data-id="' . $item . '">Delete</a></p></div><br/>';
        }

        echo "<hr/>";

        ?>
    <?php } else if ("config" ==  $_GET["action"]) {?>
        <h2>Config management:</h2>

        <?php
        require_once("classes/ConfigLoader.php");

        $confLoader = new ConfigLoader();
        $items = $confLoader->getDatFileNames();

        foreach($items as $item) {
            echo '<div class="configs"><p><a class="data-ref" href=./dat_configs/' . $item . ' target="_blank">'
                . basename($item) . '</a>';
            echo '<a href="#" style="float: right;" class="cfremover" data-id="' . $item . '">Delete</a></p></div><br/>';
        }

        echo "<hr/>";

        ?>
        <form enctype="multipart/form-data" action="./dat_loader.php" method="POST">
            <div><b>New config file:</b><input style="float: right; width: 40px;" type="submit" value="Add" />  </div>
            <input id="dat_loader" name="upfile" type="file" accept=".dat" />
        </form>

        <hr>

        <button id="reloader">Reload calculation options</button>
    <?php } else {
        header("Location: " . SITE_URL . "index.php");
        exit();
    }
    ?>

</div>

<div id="error"><?php echo $errMsg ?></div>
<div id="info"><?php echo $infoMsg ?></div>

</body>
</html>
