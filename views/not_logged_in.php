<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Calculation</title>
    <link rel="stylesheet" type="text/css"  href="/css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
    <link rel="stylesheet" type="text/css"  href="/css/style.css"/>
    <script src="/js/jquery-2.1.1.min.js"></script>
    <script src="/js/jquery-ui-1.10.4.custom.min.js"></script>
    <script>
        $(function() {
            $("#loginform").dialog({
                dialogClass: "no-close",
                closeOnEscape: false,
                resizable: false,
                title: "Enter login/password:",
                minWidth: 350,
                resize: function( event, ui ) {}
            });

            var $err = $("#error");
            if ($err.html())
            {
                $err.show();
                setTimeout(function() {$err.fadeOut("slow") }, 3000);
            }

            var $info = $("#info");
            if ($info.html())
            {
                $info.show();
                setTimeout(function() {$info.fadeOut("slow") }, 3000);
            }
            $("a").button();
        });
    </script>
</head>
<?php

$errMsg = "";
$infoMsg = "";

if (isset($login)) {
    if ($login->errors) {
        foreach ($login->errors as $error) {
            $errMsg .= $error . "<br/>";
        }
    }
    if ($login->messages) {
        foreach ($login->messages as $message) {
            $infoMsg .= $message . "<br/>";
        }
    }
}
?>
<body>
<a href=".." class="nav-link">Main page</a>
<div id="error"><?php echo $errMsg ?></div>
<div id="info"><?php echo $infoMsg ?></div>
<!-- login form box -->
<form method="post" action="index.php" id="loginform">

    <label for="login_input_username">Users login:</label><br/>
    <input id="login_input_username" class="login_input" type="text" name="user_name" required /><br/>

    <label for="login_input_password">Password:</label><br/>
    <input id="login_input_password" class="login_input" type="password" name="user_password" autocomplete="off" required /><br/>

    <input type="submit"  name="login" value="Enter" />
</form>

</body>
</html>
