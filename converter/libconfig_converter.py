# -*- coding: utf-8 -*-

from pylibconfig2 import *

__common_types = (str, int, long, float, bool)

def __parse_conf_arr_item(arr):
    if isinstance(arr, __common_types):
        return arr
    
    if isinstance(arr, ConfGroup):
        return libconfig_to_json(str(arr)[1:-1])
    
    result = []
    for i in arr:
        result.append(__parse_conf_arr_item(i))
        
    return result

def __parse_json_arr_item(arr):
    if isinstance(arr, __common_types):
        return arr
    
    if isinstance(arr, unicode):
        return str(arr)
    
    if isinstance(arr, dict):
        return __parse_json_dict(arr)
    
    result = ConfList()
    for i in arr:
        result.append(__parse_json_arr_item(i))    
        
    return result

def __parse_json_dict(obj):
    if isinstance(obj, __common_types):
        return obj
    
    result = ConfGroup() 
    for key in obj:
        value = obj[key]
        if isinstance(value, unicode):
            result.set(key, str(value))
        elif isinstance(value, __common_types):
            result.set(key, value)
        elif isinstance(value, list):
            result.set(key, __parse_json_arr_item(value))
        elif isinstance(value, dict):
            result.set(key, __parse_json_dict(value))
            
    return result
        

def libconfig_to_json(config_data):
    conf = Config(config_data)
    result = {}
    
    for key in conf.keys():
        value = conf.lookup(key)
        if isinstance(value, __common_types):
            result[key] = value
        elif isinstance(value, (ConfArray, ConfList)):
            result[key] = __parse_conf_arr_item(value)
        elif isinstance(value, ConfGroup):
            result[key] = libconfig_to_json(str(value)[1:-1])
        else:
            pass
    
    return result

def json_to_libconfig(json_data):
    result = Config('')

    data = __parse_json_dict(json_data)
    for key in data.keys():
        result.set(key, data.get(key))
    
    return result