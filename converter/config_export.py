# -*- coding: utf-8 -*-

import os
from os.path import basename
import json
from pylibconfig2 import Config
import codecs

def main():
    import_dir = "/var/www/html/dat_configs"
    for filename in os.listdir(import_dir):
        try:
            f = open(os.path.join(import_dir, filename), "r")
            data = '\n'.join(f.readlines())
            f.close()
        
            conf = Config(data)
            result = []
            
            for i in range(0, len(conf.export)):

                min_val = conf.export[i].min
                max_val = conf.export[i].max
                step = conf.export[i].step
                name = conf.export[i].name
                default = conf.export[i].default

                params = conf.export[i].params               

                for j in range(0, len(params)):    
                    result.append({
                        'path': params[j][0],
                        'value': default,
                        'units': conf.lookup(params[j][1]),
                        'min' : min_val,
                        'max' : max_val,
                        'step' : step,
                        'name' : name,
                        'type': "num"
                    })
            
            print(filename)
            jname = os.path.splitext(filename)[0]
            out = open("/var/www/html/json_configs/%s" % jname , "w")
            out.write(json.dumps(result, sort_keys=True, indent=4))
            out.close()
        except IOError as e:
            print ("Error %s %s" % (filename, e))   

if __name__ == '__main__':
    main()
    