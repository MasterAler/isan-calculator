import zmq
import json

class Client:
    def __init__(self):
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect('tcp://127.0.0.1:43000')

    def get(self):
        self.socket.send(json.dumps({
            'command' : 'load_study_program', 
            'args' : {'params': 123}
        }).encode('utf8'))
        
        print("FOO")
        return self.socket.recv()

    def set(self, data):
        self.socket.send(json.dumps({
                'command' : 'save_study_program', 
                'args' : {'data' : data}
            }).encode('utf8'))
        
        return self.socket.recv()
    
c = Client()

def d():
    print(c.get())
    #c.get()
    