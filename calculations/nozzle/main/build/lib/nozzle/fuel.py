from calcmanager import calcapi

handlers = {}

def append(new_handlers):
    for (k, v) in new_handlers.items():
        if k not in handlers:
            handlers[k] = v
        else:
            print ("DUPLICATE HANDLER: %s") % k
            
append(calcapi.handlers)