#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import zmq
import logging
import logging.handlers
import argparse
import sys
from fuel import handlers

# Deafults
LOG_FILENAME = "/tmp/calculator.log"
LOG_LEVEL = logging.INFO  # Could be e.g. "DEBUG" or "WARNING"


# Make a class we can use to capture stdout and sterr in the log
class MyLogger(object):
    def __init__(self, logger, level):
        """Needs a logger and a logger level."""
        self.logger = logger
        self.level = level

    def write(self, message):
        # Only log if there is a message (not just a new line)
        if message.rstrip() != "":
            self.logger.log(self.level, message.rstrip())

# Configure logging to log to a file, making a new file at midnight and keeping the last 3 day's data
# Give the logger a unique name (good practice)
logger = logging.getLogger(__name__)


def config_logger():
    # Set the log level to LOG_LEVEL
    logger.setLevel(LOG_LEVEL)
    # Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
    handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
    # Format each log message like this
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    # Attach the formatter to the handler
    handler.setFormatter(formatter)
    # Attach the handler to the logger
    logger.addHandler(handler)


def run():
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    #socket.bind('tcp://127.0.0.1:43000')
    socket.bind('tcp://37.48.78.27:43000')

    while True:
        data = socket.recv().decode('utf8')
        try:
            logger.info(data.encode("utf8"))  # fixes win fixed byte encoding
            commandlet = json.loads(data)
            command = commandlet["command"]
            if command in handlers:
                socket.send_string(handlers[command](**commandlet['args']))
            else:
                logger.error(b'bad fuel')
                socket.send(b'["bad fuel"]')
        except Exception as e:
            logger.error(e)
            socket.send(bytearray('Grats. You broke it. %s' % str(e), 'utf-8'))

if __name__ == '__main__':
    # Replace stdout with logging to file at INFO level
    #sys.stdout = MyLogger(logger, logging.INFO)
    # Replace stderr with logging to file at ERROR level
    #sys.stderr = MyLogger(logger, logging.ERROR)
    parser = argparse.ArgumentParser(description="Calculator service")
    parser.add_argument("-l", "--log", help="file to write log to (default '" + LOG_FILENAME + "')")
    args = parser.parse_args()
    if args.log:
        LOG_FILENAME = args.log
    config_logger()
    run()