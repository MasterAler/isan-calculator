#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import zmq
from fuel import handlers


def run():
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.bind('tcp://127.0.0.1:43000')

    while True:
        data = socket.recv().decode('utf8')
        try:
            print (data.encode("utf8"))  # fixes win fixed byte encoding
            commandlet = json.loads(data)
            command = commandlet["command"]
            if command in handlers:
                socket.send_string(handlers[command](**commandlet['args']))
            else:
                print (b'bad fuel')
                socket.send(b'["bad fuel"]')
        except Exception as e:
            print(e)
            socket.send(b'Grats. You broke it.')


if __name__ == '__main__':
    run()