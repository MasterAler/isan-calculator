# -*- coding: utf-8 -*-

import uuid
import hashlib
import codecs
import json
import os
import subprocess
from pylibconfig2 import *
from os.path import basename
import datetime
import shlex
import shutil
import tarfile
import requests
import time

CALC_DIR = r'/var/www/html/calculations/calc/'
RESULT_DIR = r'/var/www/html/calculations/calc/ResultData/'
CALC_FILENAME = CALC_DIR + 'bubble'
DAT_DIR = r'/var/www/html/dat_configs/'
JSON_DIR = r'/var/www/html/json_configs/'
PARAM_DIR = r'/var/www/html/calculations/params/'
OUT_DIR = r'/var/www/html/archive/'

LOCK_FILENAME = '/LOCK'

STATUS_OK = 0
CONFIG_NOT_FOUND = 1
FATAL_ERROR = 2


def launch(params):
    config_filename = JSON_DIR + str(params["filename"])
    dat_filename = DAT_DIR + str(params["filename"]) + '.dat'
    
    if not os.path.exists(config_filename):
        return json.dumps({'status': CONFIG_NOT_FOUND, 'info': config_filename})     
    if not os.path.exists(dat_filename):
        return json.dumps({'status': CONFIG_NOT_FOUND, 'info': dat_filename})  

    try:
        with open(config_filename, 'rb') as f:
            local_params = json.loads(codecs.getreader("utf-8")(f, errors="replace").read()) 
        
        with open(dat_filename, 'rb') as g:
            conf = Config(codecs.getreader("utf-8")(g, errors="replace").read()) 
    except IOError as e:
        return json.dumps({'status': FATAL_ERROR, 'info': config_filename})
    
    vars = params["vars"]
    for param in local_params:
        name = param["name"]
        if not name in vars.keys():
            continue
            
        conf.setup(param["path"], vars[name])
     
    with open(PARAM_DIR + params["filename"] + '.dat', 'w') as out:
        out.write(str(conf))

    open(OUT_DIR + params["user"] + LOCK_FILENAME, 'a').close()

    command = '%s --config=%s' % (CALC_FILENAME, PARAM_DIR + basename(dat_filename))
    subprocess_handle = subprocess.Popen(shlex.split(command), cwd=CALC_DIR, shell=False, stdout=subprocess.PIPE)
    subprocess_handle.wait()
    
    lres_name = str(params["filename"]) + "_" + datetime.datetime.now().strftime('%d.%m.%y_%H:%M:%S')
    res_dir = OUT_DIR + params["user"] + '/' + lres_name
    if not os.path.exists(res_dir):
        os.mkdir(res_dir)
        shutil.chown(res_dir, 'apache')
    
    result = subprocess_handle.communicate()[0]
    with open(os.path.join(res_dir, "stdout"), 'wb') as result_file:
        result_file.write(result)
        
    for filename in os.listdir(RESULT_DIR):
        try:
            if os.path.splitext(filename)[1] in ['.txt', '.vtk']:
                shutil.move(os.path.join(RESULT_DIR, filename), os.path.join(res_dir, filename))
                shutil.chown(os.path.join(res_dir, filename), 'apache')
        except IOError as e:
            print ("Error %s %s" % (filename, e)) 
            return json.dumps({'status': FATAL_ERROR, 'info': filename})
    
    tar_name = os.path.join(OUT_DIR + params["user"], "%s.tar.gz" % lres_name)
    tar = tarfile.open(tar_name, "w:gz")
    tar.add(res_dir, arcname=lres_name)
    tar.close()
    shutil.chown(tar_name, 'apache')

    # shutil.rmtree(res_dir)
    os.remove(OUT_DIR + params["user"] + LOCK_FILENAME)

    # time.sleep(2)
    # r = requests.post('http://91.235.244.101/file_loader.php', files={"upfile" : open(tar_name, 'rb')}, data={"user": params["user"]})
    # with open(os.path.join(OUT_DIR, "resp"), 'wb') as result_file:
    #     result_file.write(bytes(r.text, 'UTF-8'))
   
    return json.dumps({'status': STATUS_OK})
