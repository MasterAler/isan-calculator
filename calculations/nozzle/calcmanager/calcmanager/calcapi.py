# -*- coding: utf-8 -*-

from calcmanager.calcmanager import launch


handlers = {
    'launch': lambda params: launch(params),
}

