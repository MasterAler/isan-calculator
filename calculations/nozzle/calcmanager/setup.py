try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'calc server',
    'author': 'aler',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'My email.',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['calcmanager'],
    'scripts': [],
    'name': 'calc-launcher'
}

setup(**config)