import zmq
import json

class Client:
    def __init__(self):
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        # self.socket.connect('tcp://91.235.244.101:43000')
        self.socket.connect('tcp://127.0.0.1:43000')

    def launch(self):
        self.socket.send(json.dumps({
            'command' : 'launch', 
            'args' : {"params":{"filename":"diod_test","vars":{"pressure":"2.8"},"user":"admin"}}
        }).encode('utf8'))
        
        print("FOO")
        return self.socket.recv()
    
c = Client()

def main():
    print(c.launch())
    
if __name__ == '__main__':
    main()
    
    