import sys
import os

def line_to_array(line):
    line = line.lstrip()
    l = line.split(':')
    t = 0
    if(line[0] == '#'):
        ltype = 'comment'
    if(line[0] != '#'):
        ltype = 'numeric'
        t = float(l[0])

    return ltype,t,line

num_args = len(sys.argv)
if(num_args < 3):
    print '! error: please insert arguments: <filename> <step>'
#    raw_input("Press enter to continue")
    sys.exit()
    
#print 'num args = ',num_args
#raw_input("Press enter to continue")


if(sys.argv[1] == '' or sys.argv[2] == ''):
    print '! error: please insert arguments: <filename> <step>'
#    raw_input("Press enter to continue")
    sys.exit()

# - - - - - USING ARGUMENTS - - - - - 
input_file  = sys.argv[1]
step_str = sys.argv[2]
step = int(step_str)

# - - - - - SET FILENAMES - - - - - 
input_file_name = os.path.basename(input_file)
k = input_file_name.find('.')
output_file = '../post_process/' + input_file_name[0:k] + '_every_' + step_str + input_file_name[k:]

# - - - - - CREATE DIR IF NOT EXISTS - - - - - 
output_dir = os.path.dirname(output_file)
print 'dir       : ',output_dir
print 'dir exists: ',os.path.exists(output_dir)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)
    
print 'input_file     : ',input_file
print 'input_file_name: ',input_file_name
print 'output_file: ',output_file
print 'step       = ',step
#raw_input("Press enter to continue")


f_in = open(input_file)
f_out = open(output_file, 'w')


# ------- write header -------
sbuf_1 = f_in.readline()
sbuf_1 = sbuf_1.lstrip()
f_out.write("%s" % sbuf_1)
sbuf_1 = f_in.readline()
sbuf_1 = sbuf_1.lstrip()
f_out.write("%s" % sbuf_1)
# --- write first data line ---
sbuf_1 = f_in.readline()
sbuf_0 = sbuf_1
f_out.write("%s" % sbuf_1)
# -----------------------------

flag_eof = 0
num = 0
while flag_eof <= 0:
    sbuf_1 = f_in.readline()
    if not sbuf_1:
        flag_eof = 1
        if (num != 0):
            f_out.write("%s" % sbuf_0)
    else:    
        num = num + 1
        symb1 = sbuf_1.lstrip()[0]
        if (num >= step and symb1 != '#'):
            num = 0
            f_out.write("%s" % sbuf_1)
        sbuf_0 = sbuf_1
        
#raw_input("Press enter to continue")

f_in.close()
f_out.close()        


