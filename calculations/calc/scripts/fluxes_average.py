import sys
import os


balance_file  = '../ResultData/Balance.txt'
pulses_file = '../ResultData/pulses_list.txt'
output_file = '../post_process/averaged_fluxes.txt'
tables_file = '../post_process/fluxes_tables.txt'

output_dir = os.path.dirname(output_file)
#print 'dir       : ',output_dir
#print 'dir exists: ',os.path.exists(output_dir)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

#raw_input("Press enter to continue")



def line_to_array(line,symb):
    l = line.split(symb)
    arr = []
    for i in xrange(len(l)-1):
        arr.append(float(l[i]))   
    return arr



# ============== 1 STEP:     defining t_1 and t_2 ====================
fpulses  = open(pulses_file)

flag_eof = 0
t_c = 0.0
t_1 = 0.0
t_2 = 0.0

batch_pulses = -1
pulse_num = -1

while flag_eof <= 0:
    sbuf = fpulses.readline()
    if not sbuf:
        flag_eof = 1
    else:
        sbuf = sbuf.lstrip()              # readline 
        slen = len(sbuf)                  # check line is not empty
        if (slen > 0):                    #    .
            symb1 = sbuf.lstrip()[0]      #    . get 1 symbol
            
        if (symb1 == '#' and slen > 0):   # 1. HEADER CASE: -> null
            batch_num_c = -1
            batch_num_1 = -1
            batch_num_2 = -1
            batch_pulses = -1
            pulse_num = -1

        if (symb1 != '#' and slen > 0):   # 2. DATA CASE: -> process
            arr_c = line_to_array(sbuf,';')
            t_c = arr_c[0]

            if(pulse_num >= 0 and batch_pulses < 0): # counting pulses if first full batch
                pulse_num = pulse_num + 1
                
            if(batch_num_c == -1):          # 1 step data input
                batch_num_c = arr_c[3]

            if(batch_num_c != arr_c[3]):    # start next new batch
                batch_num_c = arr_c[3]
                if(pulse_num > 0 and batch_pulses < 0):
                    batch_pulses = pulse_num

                if(batch_num_1 == -1):      #  if first new batch -> batch_num_1
                    batch_num_1 = arr_c[3]
                    pulse_num = 0
                    t_1 = arr_c[0]
                                        
                t_2 = arr_c[0]
                batch_num_2 = arr_c[3]
fpulses.close()
pulse_data_size = len(arr_c)

batch_count      = batch_num_2 - batch_num_1
batch_length     = (t_2 - t_1)/batch_count
pulses_frequency = batch_pulses/batch_length



#print 'batch_length = ','% .17e' %batch_length;
#print 'batch_pulses = ',batch_pulses;
#print 'batch_count      = ',batch_count;
#print 'pulses_frequency = ',pulses_frequency;
#print
#print 'average from  t = ',t_1;
#print 'average until t = ',t_2;
#print '    from  batch = ',batch_num_1;
#print '    until batch = ',batch_num_2;

#raw_input("Press enter to continue")

# ============== 2 STEP:     get averaged source values ====================
fpulses  = open(pulses_file)

flag_eof = 0
t_start = t_1
t_end   = t_2

t_curr = -1.0
t_prev = -1.0
dt = 0.0

arr_prev = []
arr_curr = []
arr_average = []
arr_names = []

for i in range(pulse_data_size):
    arr_prev.append(0.0)
    arr_curr.append(0.0)
    arr_average.append(0.0)
    
while t_curr < t_end:
    sbuf = fpulses.readline()
    if not sbuf:
        flag_eof = 1
    else:
        sbuf = sbuf.lstrip()              # delete left spaces
        slen = len(sbuf)                  # check line is not empty
        if (slen > 0):                    #    .
            symb1 = sbuf.lstrip()[0]      #    . get 1 symbol

        if (symb1 == '#'):                # 1. HEADER CASE: -> get names
            sbuf = sbuf[1:len(sbuf)-1]
            sbuf = sbuf.lstrip()
            arr_names = sbuf.split(';')
            for i in range(pulse_data_size):
                arr_names[i] = arr_names[i].lstrip()
            
        if (symb1 != '#' and slen > 0):   # 2. DATA CASE: -> process
            arr_curr = line_to_array(sbuf,';')
            t_curr = arr_curr[0]

            if(t_curr >= t_start):
                dt = dt + (t_curr-t_prev)
                for i in range(1,pulse_data_size):
                    arr_average[i-1] = arr_average[i-1] + arr_prev[i-1]
                arr_prev = arr_curr
            t_prev = t_curr
fpulses.close()

for i in range(1,pulse_data_size):
    arr_average[i-1] = arr_average[i-1]/(batch_pulses*batch_count)

#for i in range(1,pulse_data_size):
#    print '%s' %arr_names[i-1].ljust(20),'=', '% .17e' % arr_average[i-1]
#print
pulse_energy_average = arr_average[1]
pulse_mass_average   = arr_average[2]

#raw_input("Press enter to continue")








fbalance = open(balance_file)


num_chamber_integr = -1
num_surface_fluxes = -1
num_all_variables = -1          # not including time
flux_location_names = []
flux_type_names = []


# - - - get names of fluxes from header - - -
num = 0
while (num < 2 ):
    sbuf = fbalance.readline()
    slen = len(sbuf)                  # check line is not empty
    symb1 = '0'
    if (slen > 0):                    #    .
        symb1 = sbuf.lstrip()[0]      #    . get 1 symbol
   
    line = sbuf.split(':')
    llen = len(line)
    sbuf = sbuf.replace(':',';')
    if (symb1 == '#'):
        num = num + 1
        sbuf = sbuf[1:len(sbuf)-1]
        sbuf = sbuf.lstrip()
        arr_names = sbuf.split(';')
        for i in range(len(arr_names)):
            arr_names[i] = arr_names[i].lstrip()

        if(num_chamber_integr == -1):
            num_chamber_integr = llen - 2        # excluding time
            num_all_variables = len(arr_names)-1 # not including time
            num_surface_fluxes = num_all_variables - num_chamber_integr
#            print 'num_all_variables = ',num_all_variables
            for i in range(num_all_variables-1):
                flux_location_names.append('')
                flux_type_names.append('')

        if(num == 1):
            for i in range(num_all_variables-1):
                flux_location_names[i] = arr_names[i+1]
        if(num == 2):
            for i in range(num_all_variables-1):
                sbuf = arr_names[i+1]
                k = sbuf.find('-')
                flux_type_names[i] = sbuf[k+1:]


#for i in range(1,num_all_variables):
#    print i,'location: ',flux_location_names[i-1]
#print
#for i in range(1,num_all_variables):
#    print i,'type: ',flux_type_names[i-1]
#raw_input("Press enter to continue")
            

            

                  
# - - - get averaged fluxes - - - 

flag_eof = 0
t_prev = -1.0
t_curr = -1.0
var_prev = []
var_curr = []
var_aver = []

mass_start = 0.0
mass_end = 0.0
en_start = 0.0
en_end = 0.0

for i in range(1,num_all_variables):
    var_prev.append(0.0)
    var_curr.append(0.0)
    var_aver.append(0.0)


while (flag_eof <= 0 and t_curr < t_end):
    sbuf = fbalance.readline()
    if not sbuf:
        flag_eof = 1
    else:
        sbuf = sbuf.lstrip()
        slen = len(sbuf)
        if (slen > 0):
            symb1 = sbuf.lstrip()[0]
            
        if (symb1 != '#' and slen > 0):
            line = sbuf.split(':')
            sbuf = sbuf.replace(':',';')
            arr = line_to_array(sbuf,';')

            if(arr[0] >= t_start and arr[0] <= t_end):
                t_curr = arr[0]
                var_curr = arr[1:]                
                if(t_prev < 0):
                    en_start   = var_curr[0]                
                    mass_start = var_curr[1]
                    t_prev = t_curr

                for i in range(1,num_all_variables):
                    var_aver[i-1] = var_aver[i-1] + var_curr[i-1]*(t_curr-t_prev)
                    
                en_end   = var_curr[0]                
                mass_end = var_curr[1]
                t_prev = t_curr
                var_prev = var_curr                            
            #raw_input("Press enter to continue")        




for i in range(1,num_all_variables):
    var_aver[i-1] = var_aver[i-1]/(t_end - t_start)
    #print flux_type_names[i-1].ljust(20),' = ', '%.17e' %var_aver[i-1]
#raw_input("Press enter to continue")    


# - - - get energy balance and conservativity - - -
summ_energy_flux = 0.0
for i in range(1,num_all_variables):
    sbuf = flux_type_names[i-1]
    k = sbuf.find('erg/s')
    if (k >= 0):
        summ_energy_flux = summ_energy_flux + var_aver[i-1]

dEdt = (en_end-en_start)/(t_end-t_start)
source_energy_power = pulse_energy_average*pulses_frequency
res_energy = -summ_energy_flux + dEdt - source_energy_power

#print
#print ' - - - - - energy fluxes - - - - - '
#print 'summ_energy_flux     = ', '%.17e' %summ_energy_flux
#print 'dE/dt                = ', '%.17e' %dEdt
#print 'source_energy_power  = ', '%.17e' %source_energy_power
#print 'result               = ','%.17e' %res_energy




# - - - get mass balance and conservativity - - -
summ_mass_flux = 0.0
for i in range(1,num_all_variables):
    sbuf = flux_type_names[i-1]
    k = sbuf.find(' g/s')
    if (k >= 0):
        summ_mass_flux = summ_mass_flux + var_aver[i-1]

dMdt = (mass_end-mass_start)/(t_end-t_start)
source_mass_power = pulse_mass_average*pulses_frequency
res_mass = -summ_mass_flux + dMdt - source_mass_power

#print
#print ' - - - - - mass fluxes - - - - - '
#print 'summ_mass_flux    = ', '%.17e' %summ_mass_flux
#print 'dM/dt             = ', '%.17e' %dMdt
#print 'source_mass_power = ', '%.17e' %source_mass_power
#print 'result            = ','%.17e' %res_mass

#raw_input("Press enter to continue")    

fbalance.close()




# - - - OUTPUT - - -

ffluxes  = open(output_file, 'w')

#f.write("%s \n" % img_name)

ffluxes.write("%s \n" % ' - - - firing pettern - - - ')
ffluxes.write("%s % .17e \n" %('batch_length = ', batch_length))
ffluxes.write("%s %2d \n"    %('batch_pulses = ', batch_pulses))
ffluxes.write("%s %2d \n"    %('batch_count      = ', batch_count))
ffluxes.write("%s %.2f \n"   %('pulses_frequency = ',pulses_frequency))
ffluxes.write("\n")              

ffluxes.write("%s \n" % ' - - - time interval - - - ')
ffluxes.write("%s % .17e \n" %('average from  t = ',t_1))
ffluxes.write("%s % .17e \n" %('average until t = ',t_2))
ffluxes.write("%s %2d \n"    %('    from  batch = ',batch_num_1))
ffluxes.write("%s %2d \n"    %('    until batch = ',batch_num_2))

ffluxes.write("\n") 
ffluxes.write("%s \n" % ' - - - averaged source - - - ')
ffluxes.write("%s % .17e \n" %('pulse_energy_average = ', pulse_energy_average))
ffluxes.write("%s % .17e \n" %('pulse_mass_average   = ', pulse_mass_average))

ffluxes.write("\n")
ffluxes.write("\n")
ffluxes.write("%s \n" % ' - - - - - energy fluxes - - - - - ')
ffluxes.write("%s % .17e \n" %('summ_energy_flux     = ', summ_energy_flux))
ffluxes.write("%s % .17e \n" %('dE/dt                = ', dEdt))
ffluxes.write("%s % .17e \n" %('source_energy_power  = ', source_energy_power))
ffluxes.write("%s % .17e \n" %('result               = ', res_energy))

ffluxes.write("\n")
ffluxes.write("%s \n" % ' - - - - - mass fluxes - - - - - ')
ffluxes.write("%s % .17e \n" %('summ_mass_flux    = ', summ_mass_flux))
ffluxes.write("%s % .17e \n" %('dM/dt             = ', dMdt))
ffluxes.write("%s % .17e \n" %('source_mass_power = ', source_mass_power))
ffluxes.write("%s % .17e \n" %('result            = ', res_mass))
ffluxes.write("\n")
ffluxes.write("\n")
ffluxes.write("%s \n" % ' - - - - - AVERAGED VALUES - - - - - ')
for i in range(1,num_all_variables):
    ffluxes.write("%s" % flux_location_names[i-1].ljust(20))
    sbuf = flux_type_names[i-1].ljust(20)+' = '
    ffluxes.write("%s" % sbuf)
    ffluxes.write("% .17e \n" %var_aver[i-1])

ffluxes.close()



# - - - - - TABLES OUTPUT - - - - -
ftables  = open(tables_file, 'w')

# - - - - - energy fluxes - - - - -

ftables.write("\n")
sbuf2 = 'Energy fluxes'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
sbuf2 = 'units: kW'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s \n" %sbuf2)
for i in range(num_all_variables-1):
    sbuf = flux_type_names[i]
    k = sbuf.find('erg/s')
    if (k >= 0):
        if(i <= num_chamber_integr-1):
            sbuf2 = flux_type_names[i]
            j = sbuf2.find(',')
            sbuf2 = sbuf2[0:j]
            sbuf2 = sbuf2.ljust(15)+';'
            ftables.write("%s " %sbuf2)
        else:
            sbuf2 = flux_location_names[i]
            sbuf2 = sbuf2.ljust(15)+';'
            ftables.write("%s " %sbuf2)

sbuf2 = '-summ_flux'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
sbuf2 = 'dE/dt'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
sbuf2 = 'source'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)

ftables.write("\n")



for i in range(num_all_variables-1):
    sbuf = flux_type_names[i]
    k = sbuf.find('erg/s')
    if (k >= 0):
        eps = var_aver[i]*1e-10
        if(i <= num_chamber_integr-1):
            sbuf2 = str("%.3e " %eps)
            sbuf2 = sbuf2.ljust(15)+';'
            ftables.write("%s " %sbuf2)
        else:
            #sbuf2 = str("%.5f " %eps)
            sbuf2 = str("%.3e " %eps)
            sbuf2 = sbuf2.ljust(15)+';'
            ftables.write("%s " %sbuf2)

# summ flux
eps = -summ_energy_flux*1e-10
#sbuf2 = str("%.5f " %eps)
sbuf2 = str("%.3e " %eps)
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
# dE/dt
eps = dEdt*1e-10
#sbuf2 = str("%.5f " %eps)
sbuf2 = str("%.3e " %eps)
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
# source
eps = source_energy_power*1e-10
#sbuf2 = str("%.5f " %eps)
sbuf2 = str("%.3e " %eps)
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
ftables.write("\n")

# - - - - - mass fluxes - - - - -

ftables.write("\n")
sbuf2 = 'mass fluxes'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
sbuf2 = 'units: g/s'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s \n" %sbuf2)
for i in range(num_all_variables-1):
    sbuf = flux_type_names[i]
    k = sbuf.find(' g/s')
    if (k >= 0):
        if(i <= num_chamber_integr-1):
            sbuf2 = flux_type_names[i]
            j = sbuf2.find(',')
            sbuf2 = sbuf2[0:j]
            sbuf2 = sbuf2.ljust(15)+';'
            ftables.write("%s " %sbuf2)
        else:
            sbuf2 = flux_location_names[i]
            sbuf2 = sbuf2.ljust(15)+';'
            ftables.write("%s " %sbuf2)

sbuf2 = '-summ_flux'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
sbuf2 = 'dm/dt'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
sbuf2 = 'source'
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)

ftables.write("\n")




for i in range(num_all_variables-1):
    sbuf = flux_type_names[i]
    k = sbuf.find(' g/s')
    if (k >= 0):
        eps = var_aver[i]
        #sbuf2 = str("%.6f " %eps)
        sbuf2 = str("%.3e " %eps)
        sbuf2 = sbuf2.ljust(15)+';'
        ftables.write("%s " %sbuf2)

# summ flux
eps = -summ_mass_flux
#sbuf2 = str("%.5f " %eps)
sbuf2 = str("%.3e " %eps)
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
# dE/dt
eps = dMdt
#sbuf2 = str("%.5f " %eps)
sbuf2 = str("%.3e " %eps)
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
# source
eps = source_mass_power
#sbuf2 = str("%.5f " %eps)
sbuf2 = str("%.3e " %eps)
sbuf2 = sbuf2.ljust(15)+';'
ftables.write("%s " %sbuf2)
ftables.write("\n")        



ftables.close()        


