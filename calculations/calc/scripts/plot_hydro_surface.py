import pylab as plt

time = 2.85373555024806034E-002
dt   = 0.1e-5
f = open("HydroSurfaceStat.txt")

tin = []
hydrogen = []
radius = []
ttime = 0.0

while(ttime < time):
    line = f.readline()
    ttime = float(line.split()[0])

print ttime

for line in f:
    t = float(line.split()[0])
    if(t != ttime):
        plt.plot(radius,hydrogen,label='H dose')
        plt.plot(radius,tin,label='tin dose')
        plt.yscale('log')
        plt.grid(True)
        plt.legend() 
        plt.xlabel('r, cm')
        plt.ylabel('dose, 1/cm$^2$')
        plt.title('accumulated dose over 8ms')
        plt.show()
        radius=[]
        hydrogen=[]
        tin=[]
        break
    radius.append(float(line.split()[2]))
    tin.append(float(line.split()[3]))
    hydrogen.append(float(line.split()[4]))
