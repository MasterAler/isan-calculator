#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
from paraview.simple import *
import os
import sys
import re

def make_images(path,state_file,prefix,output_dir):
    if(output_dir!=''):
        if(output_dir[len(output_dir)-1]!='/'):
            output_dir+='/'
            
    paraview.simple._DisableFirstRenderCameraReset()
    #load state, it should be preconfigured
    servermanager.LoadState(state_file)

    sources = GetSources()
    #look for source  with vtk in the name. Most probably it is the proper source file.
    for s in sources.items():
        if(re.findall('.*.vtk',s[0][0])):
            reader=s[1]
            reader.FileNames=path
            reader.FileNameChanged()

    #loop over all render views, change background colors to white and axes color to black
    #write individual images
    idx=0
    fnames=[]
    for view in GetRenderViews():
        SetActiveView(view)
        view.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
        view.Background = [1.0, 1.0, 1.0]
        for r in view.Representations:
            if hasattr(r,'TitleColor'): 
                r.TitleColor = [0,0,0]
            if hasattr(r,'LabelColor'):    
                r.LabelColor = [0,0,0]
            if hasattr(r,'EdgeColor'):
                r.EdgeColor = [0,0,0]
            if hasattr(r,'AmbientColor'):    
                r.AmbientColor = [0.0, 0.0, 0.0]
            if hasattr(r,'CubeAxesColor'):    
                r.CubeAxesColor = [0.0, 0.0, 0.0]
                
        #Render()
        fname='%sfig_%i.png' % (prefix, idx)
        WriteImage(output_dir+fname,view=view)
        fnames.append(fname)        
        idx+=1
    return {'fig_names': fnames , 'vtk_name' : path }

if __name__ =="__main__":
    parser = argparse.ArgumentParser(prog='images generator')
    parser.add_argument('--output_dir',type=str,default='')
    parser.add_argument('--state_file',type=str,default='sens_study.pvsm')
    parser.add_argument('--prefix'    ,type=str,default='')
    parser.add_argument('path', nargs=1)
    args = parser.parse_args()
    
    make_images(args.path,args.state_file,args.prefix,args.output_dir)