#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

import pylab as plt
import numpy as np
import scipy as sp
import scipy.special
import scipy.integrate
import argparse
import re

def xi_p(eta):
    return scipy.integrate.quad(
            lambda x: 1.0/sp.sqrt( 
                sp.exp(x) - 1.0 -sp.exp(x)*scipy.special.erf(sp.sqrt(x)) + 2.0/sp.sqrt(sp.pi) * sp.sqrt(x) 
            )
    ,0.0,eta)[0]

def xi_m(eta):
    return scipy.integrate.quad(
            lambda x: 1.0/sp.sqrt( 
                sp.exp(x) - 1.0 + sp.exp(x)*scipy.special.erf(sp.sqrt(x)) - 2.0/sp.sqrt(sp.pi) * sp.sqrt(x) 
            )
    ,0.0,eta)[0]

def get_V_for_I(I_den,I_sat_den,d = 0.2, T = 5.0):
    m = 9.109E-28
    k = 1.3806488E-16
    e = 4.803E-10
    T *= 11600.0
    I0=  I_sat_den /(1.6E-19)*e
    I =  I_den     /(1.6E-19)*e
    Vm = - k*T/e * sp.log(I0/I)
    L = 2.0 * (0.5 * sp.pi / (k * T))**(0.75) * m**(0.25) * sp.sqrt(I * e)
    Xm = xi_m(sp.log(I0/I))/(2.0 * L) 
    xi_target = 2.0 * L * (d - Xm)
    print "Vm:" + str(Vm)
    print "Xm:" + str(Xm)
    eta_min = 0.0001
    eta_max = 30.0
    for i in xrange(100):
        eta_middle = 0.5 * (eta_min + eta_max)     
        if(xi_p(eta_middle) < xi_target):
            eta_min = eta_middle
        else:
            eta_max = eta_middle                
        if(abs(xi_p(eta_middle) - xi_target) < 1e-6 ):
            break
        
    #print abs(xi_p(0.5 * (eta_max + eta_min)) - xi_target)
    #print 0.5 * (eta_max + eta_min)

    V = (Vm + k*T/e * 0.5 * (eta_max + eta_min)) * 300.0
    print "V:"+str(V)
    print ""
    return V


parser = argparse.ArgumentParser(prog='diod test plotter')

parser.add_argument("-H", "--height"       ,action="store",type=float,default=0.2     )
parser.add_argument("-F", "--full_current" ,action="store",type=float,default=0.5/(3.14 * 1.2 * 1.2))
parser.add_argument("-T", "--temperature"  ,action="store",type=float,default=5.0     )
parser.add_argument("-s", "--surface"      ,action="store",type=float,default=0.78500 )
parser.add_argument("-t", "--time_trashold",action="store",type=float,default=5e-9)

parser.add_argument("-O","--output"   ,action="store",type=str,dest="imname",default="imname.svg" )
parser.add_argument('-L','--legend_location',action='store',default='best')

parser.add_argument('--xlabel',action='store',default='')
parser.add_argument('--ylabel',action='store',default='')
parser.add_argument('files', nargs='+')

args = parser.parse_args()    
    

def find_value(config,value):
    r1=re.compile('\#\W*%s\W*\=(.*)' % (value))
    for line in config:
        res=r1.findall(line)
        if(len(res)>0):
            return res[0]
    return ''

def parse_file(file_name):    
    config=[]
    charge= 0.0
    time  = 0.0
    data_file=open(file_name)
    for line in data_file:
        if(line[0]=='#'):
            config.append(line)
        else:
            line=line.replace(',\n','').split(',')
        if(len(line)>1):            
            try:
                if (float(line[0])> args.time_trashold):                    
                    charge+=float(line[3])
                    time  +=float(line[1])                    
            except:
                pass        
    data_file.close()
    bias = find_value(config,'Bias')
    try:
        bias = float(bias)
        bias *= 300
    except:
        print "error in reading file "+ file_name
        exit()
        
    return [bias,charge/time]
    
bias   = np.zeros(len(args.files))
current= np.zeros(len(args.files))    

i = 0
for file_name in args.files:
    bias[i],current[i] = parse_file(file_name)
    bias[i]   =abs(bias[i])
    #нужны амперы/см^2
    current[i]=abs(current[i]) / args.surface
    i+=1

#строим теоретический график
I = np.linspace(current.min(),current.max(),10)
V = np.zeros(len(I))

for i in xrange(len(I)):
    V[i] = get_V_for_I(I_den=I[i],I_sat_den =args.full_current,d=args.height,T=5.0)

fig=plt.figure()
plt.plot(bias,current,"o",label="model")
plt.plot(V,I,label="exact solution")
plt.xlabel("bias (V)")
plt.ylabel("current density (mA/cm^2)")
plt.title("vacuum diod IV, electron dist - maxwell")
#plt.legend()
plt.grid(True)
plt.savefig(args.imname)

print "theor"
s_I=[]
s_V=[]
for i in xrange(len(I)):
    s_I.append(str(I[i]))
    s_V.append(str(V[i]))
print "I: "+",".join(s_I)    
print "V: "+",".join(s_V)

print "calculated"
s_I=[]
s_V=[]
for i in xrange(len(current)):
    s_I.append(str(current[i]))
    s_V.append(str(bias[i]))
print "I: "+",".join(s_I)    
print "V: "+",".join(s_V)


    
