#!/usr/bin/python
# -*- coding: utf-8 -*-

import pylab as plt
import matplotlib as mp
import re
import numpy as np
import argparse

        
parser = argparse.ArgumentParser(prog='diod test plotter')

parser.add_argument("-H", "--height" ,action="store",type=float,default=0.1     )
parser.add_argument("-s", "--surface",action="store",type=float,default=0.78500 )
parser.add_argument("-t", "--time_trashold",action="store",type=float,default=6e-9)

parser.add_argument("-O","--output"   ,action="store",type=str,dest="imname",default="imname.svg" )
parser.add_argument('-L','--legend_location',action='store',default='best')

parser.add_argument('--xlabel',action='store',default='')
parser.add_argument('--ylabel',action='store',default='')
parser.add_argument('files', nargs='+')

args = parser.parse_args()

def find_value(config,value):
    r1=re.compile('\#\W*%s\W*\=(.*)' % (value))
    for line in config:
        res=r1.findall(line)
        if(len(res)>0):
            return res[0]
    return ''

def parse_file(file_name):    
    config=[]
    charge= 0.0
    time  = 0.0
    data_file=open(file_name)
    for line in data_file:
        if(line[0]=='#'):
            config.append(line)
        else:
            line=line.replace(',\n','').split(',')
        if(len(line)>1):            
            try:
                if (float(line[0])> args.time_trashold):                    
                    charge+=float(line[5])
                    time  +=float(line[1])                    
            except:
                pass        
    data_file.close()
    bias = find_value(config,'Bias')
    try:
        bias = float(bias)
        bias *= 300
    except:
        print "error in reading file "+ file_name
        exit()
        
    return [bias,charge/time]
    
bias   = np.zeros(len(args.files))
current= np.zeros(len(args.files))

i = 0
for file_name in args.files:
    bias[i],current[i] = parse_file(file_name)
    bias[i]   =abs(bias[i])
    #нужны милиамперы/см^2
    current[i]=1000.0 * abs(current[i]) / args.surface
    i+=1
    
fig=plt.figure()

#строим теоретический график

V = np.linspace(0.0,1.1 * bias.max(),20)
I = np.zeros(len(V))

for i in xrange(len(V)):
    I[i] =0.001 * 2.333 / (args.height * args.height) * V[i]**(1.5)
error = np.zeros(len(bias))
for i in xrange(len(bias)):
      error[i] = (current[i] - 0.001 * 2.333 / (args.height * args.height) * bias[i]**(1.5))/current[i]

plt.plot(bias,current,"o")
plt.plot(V,I)
plt.xlabel("bias (V)")
plt.ylabel("current density (mA/cm^2)")
plt.title("vacuum diod IV characteristic, max rel error {0:.3f}".format(error.max()))
plt.grid(True)
plt.savefig(args.imname)
    
