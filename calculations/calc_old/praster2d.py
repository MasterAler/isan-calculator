#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import pylab as plt
import argparse
import re 
import matplotlib.colors
from matplotlib.colors import LogNorm
import scipy.ndimage as ndimage
import gc

parser = argparse.ArgumentParser(prog='2d plotter')
parser.add_argument('-L','--legend_location',action='store',default='best')
parser.add_argument('-N','--norm_independent',action='store_true',default=False)
parser.add_argument('--xlabel',action='store',default='')
parser.add_argument('--ylabel',action='store',default='')
parser.add_argument('-r','--radius'  ,action='store',type=float,default=0.0)
parser.add_argument('-l','--linear'  ,action='store_true',default=False)
parser.add_argument('-g','--geometry',action='store',default='generated_geometry.txt')
parser.add_argument('-m','--map_min' ,action='store',type=float,default=-1.0)
parser.add_argument('-M','--map_max' ,action='store',type=float,default=-1.0)
parser.add_argument('-E','--min_E'   ,action='store',type=float,default= 0.0)
parser.add_argument('files', nargs='+')
args = parser.parse_args()
E_threshold = args.min_E
elements = [
            [0 ,"e"],
#            [1 , "H"],
            [2 ,"$H^+$"],
            [9 ,"$H_2^+$"],
            [11,"$H_3^+$"]
            ]
#elements=[[0,'e'],[2,'$Ar^+$']
#[3,'$Ar^{++}$']
#]
#elements_names = ["e","$Ar^+$","$Ar^{+2}$","$Ar^{+3}$"]

file_name=args.geometry
fd=open(file_name)
lines = fd.readlines()
dz_line = lines[0].replace(',\n','').split(',')
z_max = 0.0
for i in xrange(len(dz_line)):
    z_max += float(dz_line[i])

dr_line = lines[1].replace(',\n','').split(',')    
r_max = 0.0
for i in xrange(len(dr_line)):
    r_max += float(dr_line[i])

k = r_max / z_max

n_cells_x = 128
n_cells_y = 128

if(k>1):
    n_cells_x = int(round(n_cells_y * k))
else:
    n_cells_y = int(round(n_cells_x / k))
    
V = np.zeros([n_cells_y,n_cells_x])

dr = (r_max)/float(n_cells_x)
dz = (z_max)/float(n_cells_y)
R0 = args.radius

n = R0/dr
for i in xrange(n_cells_x):
    for j in xrange(n_cells_y):
        V[j][i]=3.14 * dr * dr * dz * (2.0 * (i + n ) + 1.0 )

print V[0][0]
for name in args.files:    
    fd=open(name)          
    Z = np.zeros([len(elements),n_cells_y,n_cells_x])
    e_count = np.zeros(len(elements))
    for line in fd:    
        #try:
            data = line.split()
            for idx in xrange(len(elements)):
                if(int(data[0]) == elements[idx][0]): 
                    r = float(data[2]) - R0
                    z = float(data[1])
                    E = float(data[3])
                    if(E > E_threshold ):
                        w = float(data[4])
                        i = np.floor(r/dr)
                        j = np.floor(z/dz)
                        eta = (r/dr - float(i) )
                        xi  = (z/dz - float(j) )
                        try:
                            #Z[idx][j][i] += w
                            for a in xrange(2):
                                for b in xrange(2):
                                    if(i + a < n_cells_x and j + b < n_cells_y):
                                        Z[idx][j+b][i+a] += w * abs(1.0 - b - eta) * abs(1.0 - a - xi )
                            e_count[idx]+= w
                        except:
                            pass
                        #try:
                            #Z[idx][j  ][i  ] += w * (1.0 - eta) * (1.0  - xi)
                            #Z[idx][j+1][i  ] += w * (1.0 - eta) * xi 
                            #Z[idx][j  ][i+1] += w *  eta * (1.0 - xi)
                            #Z[idx][j+1][i+1] += w * xi * eta                    
                        #except:
                            #print "(%i,%i)"%(j,i)
                            #pass
                    break;
        #except:
        #    print "error in:" + line
        #    pass        
    for idx in xrange(len(elements)):
        print '%s : %.2e' % (elements[idx][1],e_count[idx])
    fd.close()      
    for idx in xrange(len(elements)):
        for i in xrange(n_cells_x):
            for j in xrange(n_cells_y):
                Z[idx][j][i] = Z[idx][j][i]/ V[j][i] + 1                
#        Z[idx] = ndimage.gaussian_filter(Z[idx], sigma=0.5, order=0)
                
    map_min = Z.min()
    if(args.map_min > 0.0):
        map_min = args.map_min
    if(map_min < 1e4):
        map_min=1e4    
    map_max = Z.max()
    if(args.map_max > 0.0):
        map_max = args.map_max
    #map_max=1.3e8
    fig = plt.figure()                                          
    fig=plt.figure()
    plt.subplots_adjust(wspace=0.5)
    plt.axes([0.0,0.0,1.0,1.0])
    plt.axis('off')        
    k = r_max/z_max
    width = 4
    height= 4
    if(k > 1):
        width = height * k                    
    else:
        height= width / k
    
    
    
    a = 2
    b = 2
    
    if(len(elements) == 2):
        a = 1
        b = 2    
        width  *= 2
    elif(len(elements) == 1):
        a = 1
        b = 1
        
    if  (width/height > 3.0):
        a = 4
        b = 1
        height *= 2
        width  /= 2
    elif(height/width > 3.0):
        a = 1
        b = 4
        width *=2
        height/=2
    fig.set_size_inches(1.1*width,height)
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05, wspace=0.3, hspace=0.3)
    time = re.findall('.*_(.*)_V_.*\.txt',name)
    if(time):
        time = float(time[0])*1e6
    else:
        time=False
    #plt.subplots_adjust(left=0.05, right=0.95, top=0.9, bottom=0.1, wspace=0.3, hspace=0.3)
    for map_id in xrange(len(elements)):
        ax = fig.add_subplot(a,b,map_id+1)
        ax.set_xlabel("R (cm)")
        ax.set_ylabel("Z (cm)")
        if(not args.linear):
            img=[]
            if(not args.norm_independent):
                img= plt.imshow(Z[map_id],origin='lower',norm=LogNorm(vmin=map_min, vmax=map_max),extent=(0,r_max,0,z_max))             
            else:
                img= plt.imshow(Z[map_id],origin='lower',norm=LogNorm(vmin=map_min),extent=(0,r_max,0,z_max))
            #ax.set_image_extent(0, 0, r_max, z_max)
            #ax.set_xlim(0,r_max)
            #ax.set_ylim(0,z_max)             
            #ax.set_title(elements[map_id][1])
            cbar = plt.colorbar(img,ax = ax)
            cbar.set_ticks([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12])            
            cbar.set_ticklabels(["$10^5$","$10^6$","$10^7$","$10^8$","$10^9$","$10^{10}$","$10^{11}$","$10^{12}$"])            
        else:
            if(not args.norm_independent):
                Z[map_id] = ndimage.gaussian_filter(Z[map_id], sigma=1.0, order=0)
                img= plt.imshow(Z[map_id],origin='lower',extent=(0,r_max,0,z_max),norm=matplotlib.colors.Normalize(vmin=map_min,vmax=map_max))
                step = (map_max - map_min)/5
                #marks=plt.arange(map_min,map_max,step)
                marks= [1e7,5e7,1e8,5e8]
                #cs2 = plt.contour(Z[map_id],marks,extent=(0,r_max,0,z_max),colors='w',linewidth=1,hold= 'on')
                #plt.clabel(cs2, inline=1,fontsize=8)
                #img= plt.imshow(Z[map_id],origin='lower',norm=matplotlib.colors.Normalize(vmin=map_min,vmax=map_max))
            else:
                img= plt.imshow(Z[map_id],origin='lower',extent=(0,r_max,0,z_max),norm    =matplotlib.colors.Normalize(vmin=map_min))
            #            
            cbar = plt.colorbar(img,ax = ax)
        if(time):
            ax.set_title(elements[map_id][1] + ('  ($%.1e\, \mu s$)'% time))
        else:
            ax.set_title(elements[map_id][1])
    
    plt.savefig(name+'.png',bbox_inches='tight',dpi=300)
    #plt.show()
    #plt.savefig('electrons_%s.png' % (('%.0f' % (time * 1e3)).zfill(6) ) ,bbox_inches='tight',dpi=300)
    plt.close(fig)
    
exit()
