#!/usr/bin/python
# -*- coding: utf-8 -*-

import pylab as plt
import matplotlib as mp
import re
import numpy as np
import argparse
from bisect import bisect_left
substrate= 'C'

def find_value(config,value):
    r1=re.compile('\#\W*%s\W*\=(.*)' % (value))
    for line in config:
        res=r1.findall(line)
        if(len(res)>0):
            return res[0]
    return ''


def parse_file(file_name,dt = 1e-10,T_smooth = 200e-9,smooth=True,signal_column_1=4,signal_column_2=5):    
    data_file=open(file_name)    
    config=[]
    X  = []
    Y  = []
    
    find_config = True
    while(find_config):
        line = data_file.readline()
        if(line[0]=='#'):
            config.append(line)
        else:
            find_config = False
            line=line.replace(',\n','').split(',')
            X.append(float(line[0]))
            Y.append(0)    
    t = X[0]        
    summ = 0
    for line in data_file:    
        line=line.replace(',\n','').split(',')
        if(len(line)>1):            
            try:                
                x = float(line[0])                
                y2 =  (float(line[6]) +  float(line[2]) - float(line[3]))
                y =  y2
                summ += y
                if( t+dt <= x ):
                    Y.append(summ)
                    X.append(x)
                    t = x                                    
            except:
                pass
    data_file.close()
    Y = plt.array(Y)
    X = plt.array(X)
    Current = plt.zeros(len(Y))   
    for i in xrange(1,len(Y)-1):
        Current[i] = (Y[i+1] - Y[i-1])/(X[i+1] - X[i-1]) 
    Current[0       ] = (Y[1] - Y[0])/(X[1] - X[0])
    Current[len(Y)-1] = (Y[len(Y)-1] - Y[len(Y)-2])/(X[len(Y)-1] - X[len(Y)-2])    
    if(smooth):
        #сгладим сигнал    
        S_function = np.zeros(2 *  len(Current) - 1)
        for i in xrange(len(Current)):
            S_function[i] = 0
        summ = 0.0
        for i in xrange(len(Y),2 * len(Y) -1 ):            
            S_function[i] = np.exp( - (i - len(Y)) * dt /T_smooth  )
            summ += S_function[i]
        norm = 1.0/summ
        for i in xrange(len(Y),2 * len(Y) -1 ):
            S_function[i] *= norm
        
        Current = np.convolve(S_function,Current,'valid')        
    
    return [X,Current,Y,config]

def parse_integrate(file_name,cat=-1):
    data_file=open(file_name)
    lines=data_file.readlines()
    data_file.close()
    config=[]
    integral = 0.0
    tmp = 0.0
    count = 0
    for line in lines:
        if(line[0]=='#'):
            config.append(line)
        else:
            line=line.replace('\n','').split(',')                        
        if(len(line)>1):            
            try:                                
                #tmp += float(line[2]) - float(line[3])
                tmp += -float(line[4]) + float(line[5])
                t = float(line[0])
                if(t > cat and cat > 0):
                    break
                #tmp += float(line[2])
                count += 1
                if(count > 100):
                    count = 0
                    integral += tmp
                    tmp = 0
            except:
                pass
   # print integral        
    return [integral,config]

def parse_integrate_column(file_name,cat=-1,column=2):
    data_file=open(file_name)
    lines=data_file.readlines()
    data_file.close()
    config=[]
    integral = 0.0
    tmp = 0.0
    count = 0
    for line in lines:
        if(line[0]=='#'):
            config.append(line)
        else:
            line=line.replace('\n','').split(',')                        
        if(len(line)>1):            
            try:                                
                tmp += float(line[column]) 
                t = float(line[0])
                if(t > cat and cat > 0):
                    break
                count += 1
                if(count > 100):
                    count = 0
                    integral += tmp
                    tmp = 0
            except:
                pass
    print integral        
    return integral
 

def parse_file_exp(file_name):
    data_file=open(file_name)
    lines=data_file.readlines()
    data_file.close()
    config=[]
    X  = np.zeros(len(lines))
    Y  = np.zeros(len(lines))
    count=0
    for line in lines:  
    
        try:         
            line=line.replace(',\n','').split()
            X [count]=(float(line[0]) - 160.0) * 1e-9                
            Y [count]=(float(line[1]) + 0.017)/ 50.0                                             
            count+=1;
        except:
            pass    
        
        #if(X[count-1] > 5e-7):
        #    X = np.resize(X,count)
        #    Y = np.resize(Y,count)
        #    return [X,Y]
    Y =np.resize(Y ,count)   
    X =np.resize(X ,count)     
    
    return [X,Y]
    
    
parser = argparse.ArgumentParser(prog='1d plotter')
parser.add_argument("-I", "--integrate" ,action="store_true",default=False)
parser.add_argument("-d", "--diff_mode" ,action="store_true",default=False)
parser.add_argument("-T", "--save_txt"  ,action="store_true",default=False)
parser.add_argument("-m", "--mean"      ,action="store_true",default=False)
parser.add_argument("-C", "--cat"       ,action="store",type=float,default=-1.0)
parser.add_argument("-k", "--mult"      ,action="store",type=float,default= 1.0)
parser.add_argument("-a", "--add"       ,action="store",type=float,default= 0.0)
parser.add_argument("-S", "--start"     ,action="store",type=float,default= 0.0)
parser.add_argument("-c", "--comparison",action="store_true",default=False)
parser.add_argument("-s", "--smooth"   ,action="store",type=float,dest="smooth",default=2e-9)
parser.add_argument("-o","--one_plot"  ,action="store_true",default=False)
parser.add_argument("-O","--output"   ,action="store",type=str,dest="imname",default="" )
parser.add_argument('-L','--legend_location',action='store',default='best')


parser.add_argument('--xlabel',action='store',default='')
parser.add_argument('--ylabel',action='store',default='')
parser.add_argument('files', nargs='+')
args = parser.parse_args()

if( args.comparison):
    data_name = args.files[0]
    exp_name  = args.files[1]
    
    Xt,Yt,integ,config = parse_file(data_name) 
    Xe,Ye        = parse_file_exp(exp_name)
    
    plt.plot(Xt*1e9,-Yt*1000.0,label='theory')
    #plt.plot(Xe*1e9-20,Ye*1000.0-0.5,label='experiment')
    plt.plot(Xe*1e9,Ye*1000.0,label='experiment') 
    plt.xlabel("time (ns)")    
    plt.ylabel("Current (mA)") 
    plt.legend(loc=args.legend_location)
    plt.grid(True)
    plt.show()
    exit()
        

if(not args.integrate):
    if(not args.one_plot):
        for i in xrange(len(args.files)):   
            data_name = args.files[i]
            X=[]
            Y=[]
            Y2=[]
            config=[]        
            X,Y,Y2,config= parse_file(data_name,T_smooth=args.smooth)        
            #отпарсили файл, теперь будем рисовать.
        
            
            #у нас есть несколько конфигурациооных строк, 
            #используем их для обозначения осей.
            dt=find_value(config,'dt')    
            if(args.xlabel):
                plt.xlabel(args.xlabel)    
            else:
                plt.xlabel(find_value(config,'xlabel'))    
            if(args.xlabel):
                plt.ylabel(args.ylabel)    
            else:    
                plt.ylabel(find_value(config,'ylabel'))    
            Bias = find_value(config,'Bias')
            try:
                Bias=float(Bias)*300
                Bias="%.1f" % Bias
            except:
                Bias=Bias+'Stat V'        
            Intensity = find_value(config,'Intensity')
            try:
                Intensity=float(Intensity)
                Intensity=str(Intensity)
            except:
                pass
            
            idx = bisect_left(X,args.start)          
            legend = 'Full charge %.3f (nC), Bias %s (V), ' % (Y2[len(Y2)-1]*1e9, Bias)
            fig=plt.figure()
            ax1=fig.add_subplot(111)
            plt.grid(True)
            ax1.plot(X[idx:]*1e6,Y[idx:]*1e3,lw=2,label=legend)                 
            ax1.set_ylabel("current, mA")
            
            
            ax2 = ax1.twinx()
            ax2.plot(X[idx:]*1e6,Y2[idx:]*1e9,'-.',lw=2,label=legend)
            ax2.set_ylabel("charge, nC")            
            plt.legend(loc=args.legend_location)
            imname=data_name.replace('.txt','.svg')
            plt.savefig(imname)
            
            if(args.save_txt):
                imname = imname + '.txt'
                f = open(imname,'w')
                for i in xrange(len(X)):
                    f.write('%e %e \n' % (X[i]*1e6,Y[i]*1e3))
                f.close()

            
            if(args.mean):
                l=50
                current = 0.0
                for i in xrange(len(Y)-l,len(Y)-1):
                    current += Y[i]
                
                current=current/float(l)
                print "(%s , %s)" % (Bias, current)
        
    if(args.one_plot): 
        fig=plt.figure()
        ax1=fig.add_subplot(111)
        ax2 = ax1.twinx()
        ax2.set_ylabel("charge, nC")
        ax1.set_ylabel("current, mA")
        for i in xrange(len(args.files)):   
            data_name = args.files[i]
            X=[]
            Y=[]
            Y2=[]
            config=[]        
            X,Y,Y2,config= parse_file(data_name,T_smooth=args.smooth)        
            #отпарсили файл, теперь будем рисовать.                    
            #у нас есть несколько конфигурациооных строк, 
            #используем их для обозначения осей.            
            Bias = find_value(config,'Bias')
            try:
                Bias=float(Bias)*300
                Bias=str(Bias)
            except:
                Bias=Bias+'Stat V'        
            Intensity = find_value(config,'Intensity')
            try:
                Intensity=float(Intensity)
                Intensity=str(Intensity)
            except:
                pass
            idx = bisect_left(X,args.start)    
            legend = 'I %s mJ, Bias %s' % (Intensity, Bias)                        
            ax1.plot(X[idx:]*1e6,Y[idx:]*1000.0,label=legend)                                        
            ax2.plot(X[idx:]*1e6,Y2[idx:]*1e9,label=legend)
            print " %s , %.2e" % (Bias,Y2[len(Y2)-1])                                                            
            
        ax1.legend(loc=args.legend_location)        
        imname=args.files[len(args.files)-1].replace('.txt','.svg')
        if(args.imname):
            imname=args.imname
        ax1.grid(True)
        #ax2.grid(True)
        plt.savefig(imname)   
else:
    #пробуем проинтегрировать значения    
    X=[]
    Y=[]
    point_count = 0
    plot_label  = 'theor'
    plotted = False
    color=''
    style=''
    for name in args.files:   
        if(re.findall("label:(.+)",name)):
            plot_label=re.findall("label:(.+)",name)[0]
            X1=plt.array(X)
            Y1=plt.array(Y)
            p = X1.argsort()
            X2=X1[p]
            Y2=Y1[p]
            if(color and style):
                plt.plot(X2,Y2,style,label=plot_label,color=color)
            elif(color):
                plt.plot(X2,Y2,'-o',label=plot_label,color=color)
            else:
                plt.plot(X2,Y2,'-o',label=plot_label)
            plotted = True
            X=[]
            Y=[]
        elif(re.findall("color:(.+)",name)):
            color=re.findall("color:(.+)",name)[0]
        elif(re.findall("style:(.+)",name)):
            style=re.findall("style:(.+)",name)[0]
        else:           
            value, config = parse_integrate(name,args.cat)
            value*=args.mult
            value-=args.add
            bias = find_value(config,'Bias')
            try:                
                X.append(float(bias)*300.0)
                Y.append(float(value) * 1e9)
                print "%.1f, %.2f" %(X[len(X)-1],Y[len(Y)-1])
            except:
                print "error reading header in %s , exiting" % name
                exit()                        
                       
    if(not plotted):
        X1=plt.array(X)
        Y1=plt.array(Y)
        p = X1.argsort()
        X2=X1[p]
        Y2=Y1[p]
        if(color):
            plt.plot(X2,Y2,'-o',label=plot_label,color=color)            
        else:
            plt.plot(X2,Y2,'-o',label=plot_label)
            
        plotted = True
        X=[]
        Y=[]
        
    if(substrate=='Ru'):
        #эксперементальные значния рутений
        Y2=plt.array([-78.773,-69.9479,-83.4924,-63.3497,-60.8664,-73.6941,-66.4421,-60.2386,-53.5781,-42.5197,-30.5328,-19.1282,-11.7178])
        Y2*=1.0/50.0
        X2=plt.array([-200,-190,-179.231,-160,-150.769,-139.615,-122.692,-94.6154,-70.7692,-46.5385,-26.1538,-8.84615,0])        
        plt.plot(X2,Y2,'--^',label="exp, 0.0 Pa", color="blue")
        
        #X2=plt.array([0.0   ,-50.0   ,-100.0 ,-150.0 ,-200.0]) 
        #Y2=plt.array([-0.1109,-0.4621,-0.6785,-0.6917,-0.8905])
        #Y2*=1.0/50.0 * 100.0
        #plt.plot(X2,Y2,'--^',label="experimental, 0.0 Pa")
        
        X2=plt.array([0.0   ,-20.0 ,-50.0,-70.0  ,-100.0 , -120.0 ,-150.0 , -180.0, -200.0 ])
        Y2=plt.array([-0.141,-0.326,-0.53,-0.6756,-0.72  , -0.7344,-0.9093, -0.9542, -0.8028 ])
        Y2*=1.0/50.0 * 100.0
        plt.plot(X2,Y2,'--^',label="exp, 2.8 Pa",color="green")
        
        
        
        X2=plt.array([0.0    ,-20.0  ,-50.0  , -70.0 ,-90.0  , -100.0 ,-120.0 ,-130.0 ,-150.0 ,-180.0  , -200.0 ])
        Y2=plt.array([-0.1908,-0.3623,-0.7947,-0.8806,-0.9272, -0.9091,-1.1193,-0.9883,-0.9747,-1.1199 , -1.1465])
        Y2*=1.0/50.0 * 100.0
        plt.plot(X2,Y2,'--^',label="exp, 11.3 Pa",color="red")
        
        X2=plt.array([0.0   ,-25.0,-50.0, -75.0 ,-100.0 , -125.0,-150.0, -175.0, -200.0 ])
        Y2=plt.array([-28.0,-68.0,-112.0,-148.0,-168.0 , -180.0,-176.0, -184.0, -200.0 ])
        Y2*=1.0/50.0
        plt.plot(X2,Y2,'--^',label="exp, 45.0 Pa",color="green")
        
        
        #X2 = plt.arange(-300,0,10)        
        #Y2 = - 0.01*(9e-8 + 9e-9 * abs(X2)**(0.73) )*1e9
        #plt.plot(X2,Y2,'--^',label="experimental")
    elif(substrate=='C'):
        #Экспериментальные значения с углерода
        plot_vacuum = 1
        plot_3pa    = 1
        plot_11pa   = 1
        plot_22pa   = 1
        plot_45pa   = 1
        if(plot_vacuum): 
            X=np.array([-200.0,-175.0,-150.0,-125.0,-100.0,-75.0,-50.0,-25.0,-10.0, 0.0])
            Y=np.array([-54.0 ,-53.0 ,-49.3 ,-51.5 ,-52.2 ,-49.0,-43.0,-32.4,-21.6,-8.5])
            plt.plot(X,Y/50.0,'--^',label='C 0pa',color='blue')
        if(plot_3pa):
            X=np.array([-200.0,-175.0,-150.0,-125.0,-100.0,-75.0,-50.0,-25.0,-10.0, 0.0])
            Y=np.array([-64.0894,-65.8446,-64.8147,-66.8744,-62.6221,-57.4418,-48.2262,-34.1902,-24.473,-15.49])
            plt.plot(X,Y/50.0,'--^',label='C 2.8pa' ,color='green')
        if(plot_11pa):
            X=np.array([-200.0,-175.0,-150.0,-125.0,-100.0,-75.0,-50.0,-25.0,-10.0, 0.0])
            Y=np.array([-78.1377,-75.6057,-77.3779,-74.864,-75.2078,-71.8758,-65.1414,-45.3703,-31.0782,-18.9687])
            plt.plot(X,Y/50.0,'--^',label='C 11.2pa',color='red')
        if(plot_22pa):
            X=np.array([-200.0,-175.0,-150.0,-125.0,-100.0,-75.0,-50.0,-25.0,-10.0, 0.0])
            Y=np.array([-93.8612,-89.2981,-92.1337,-92.4936,-83.0708,-87.9533,-73.4954,-55.0643,-35.7648,-22.8985])    
            plt.plot(X,Y/50.0,'--^',label='C 22.4pa',color='magenta')
        if(plot_45pa):
            X=np.array([-200.0,-175.0,-150.0,-125.0,-100.0,-75.0,-50.0,-25.0,-10.0, 0.0])
            Y=np.array([-120.566,-110.848,-107.2,-103.927,-98.6118,-80.617,-77.2961,-48.5861,-30.8792,-19.0746])
            plt.plot(X,Y/50.0,'--^',label='C 45.0pa',color='black')
    
    plt.ylim(ymax=0.0)
    plt.xlim(xmax=0.0,xmin=-200.0)
    plt.grid(True)
    plt.xlabel("Voltage (V)")    
    plt.ylabel("Charge (nC)")  
    plt.legend(loc=0)
    imname=''
    if(not args.imname):
        imname='imname.svg'
    else:
        imname=args.imname
    plt.savefig(imname)
    
