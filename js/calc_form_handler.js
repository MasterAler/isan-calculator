$(function(){
    var $result = $("#result");

    $("#batch_pulse_count").on("change", function() {
        var $text = $result.text();
        $result.text($text.replace(/batch_pulse_count = [0-9]+/, "batch_pulse_count = " + $(this).val()));
    });

    $("#batch_void_count").on("change", function() {
        var $text = $result.text();
        $result.text($text.replace(/batch_void_count = [0-9]+/, "batch_void_count = " + $(this).val()));
    });

    $("#batch_max_count").on("change", function() {
        var $text = $result.text();
        $result.text($text.replace(/batch_max_count = [0-9]+/, "batch_max_count = " + $(this).val()));
    });

    $("#cross_section_data").on("change", function() {
        var $text = $result.text();
        var filename = $(this).val().substring($(this).val().lastIndexOf('\\')+1);
        $result.text($text.replace(/cross_section_data = "tracer_tables\/[A-z]+.dat/, 'cross_section_data = "tracer_tables/' + filename + ".dat"));
    });

});