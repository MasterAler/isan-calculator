#!/usr/bin/python2
# -*- coding: utf-8 -*-
import argparse
import subprocess
import os
import re
import sys
import numpy as np
sys.path.append('pylibconfig')
import pylibconfig

import dropline_and_rays_out

MAKE_PARAVIEW_IMAGES = True
MAKE_DENSITY_INTEGRALS = True# False#True

parser = argparse.ArgumentParser(prog='report generator')
parser.add_argument('--output_dir',type=str,default='')
parser.add_argument('--prefix', type=str,default='')
parser.add_argument('--state_file',type=str,default='')
parser.add_argument('--primary_focus',type=float,default = 19.0)
parser.add_argument('--intermediate_focus',type=float,default = 160.0)
parser.add_argument('path', type=str, default='')
args = parser.parse_args()

def check_for_source(fig):
    source = ''
    if(os.path.isfile(images_dir + '/' + fig +'.csv')):            
            source = r'\textattachfile{images/%s}{\small{\textcolor{black}{Datafile:} \textcolor{blue}{\detokenize{%s}}}}.' % (fig+'.csv',fig+'.csv')
    return source


if(args.path!=''):
    if(args.path[len(args.path)-1]!='/'):
        args.path+='/'

if(args.output_dir!=''):
    if(args.output_dir[len(args.output_dir)-1]!='/'):
        args.output_dir+='/'
        
if(args.output_dir == '' and args.path!=''):
    args.output_dir = args.path +  'report/'
        
images_dir = args.output_dir+'images'
results_path = args.path + 'ResultData'

if(not os.path.exists(args.output_dir)):
    os.mkdir(args.output_dir)
if(not os.path.exists(images_dir)):
    os.mkdir(images_dir)
else:
    if(not os.path.isdir(images_dir)):
        print "cant create folder %s : exist not a directory" % images_dir
        exit()
#path are ok, let go
#create tex file


cfg     = pylibconfig.libconfigFile(args.path+'config.dat')
case_id = cfg.get("case_id")

tex_file_name = args.output_dir + '/' + args.prefix + ('report_%s.tex' % case_id)
if(not args.output_dir):
    tex_file_name = args.prefix + ('report_%s.tex' % case_id)

tex_file = open(tex_file_name,'w')

#tex header
tex_file.writelines(
[r'\documentclass{article}','\n',
r'\usepackage{polyglossia}' ,'\n',
r'\setdefaultlanguage{english}','\n',
r'\setotherlanguage[spelling=modern]{russian}','\n',
r'\usepackage{fontspec}','\n',
r'\setmainfont[Ligatures=TeX]{Linux Libertine O}','\n',
r'\usepackage{geometry}','\n',
r'\geometry{a4paper, top=1.0cm, left=1.5cm, right=1.5cm, bottom=1.5cm}','\n',
r'\usepackage{booktabs}','\n',
r'\usepackage{tabularx}','\n',
r'\usepackage{color}'   ,'\n',
r'\usepackage{attachfile2}','\n',
r'\usepackage{dcolumn}','\n'
]
)


tex_file.write(r"""
\begin{document}
\title{PGI simulation report \\ \vspace{2mm}\small{id:\detokenize{%s}} \\ \vspace{2mm}\small{status: preliminary }}    
\date{\today}
\maketitle
""" % (case_id))

#header done, let read config and make table about input 
bic_file_name = cfg.get('hydro_config.bic')
pressure    = -1
temperature = -1
flow        = ''
if(bic_file_name):
    bic_file  = open(args.path+bic_file_name)
    bic_lines = bic_file.readlines()
    pressure    = -1
    temperature = -1
    flow = ''
    for i in xrange(len(bic_lines)):
        if(re.findall('#.*Initial Conditions.*',bic_lines[i])):
            l = bic_lines[i+1]
            idx = i+1
            for j in xrange(1,len(bic_lines)): 
                l = bic_lines[i+j]
                if(l[0]!='#'):
                    idx = i + j
                    break
            bcount = int(l)
            flag = False 
            for j in xrange(idx+1,len(bic_lines)): 
                l = bic_lines[j]
                if  ((l[0]!='#') and  (flag == False)):
                    flag = True
                elif((l[0]!='#') and  (flag == True)):                                
                    break        
            density = float(l.split()[0].replace('d','e'))
            pressure= float(l.split()[3].replace('d','e'))
            temperature = (pressure * 2.0)/(density * 8.3144621e7)
            pressure/=10.0 # cgs -> Pa
        if(re.findall('#.*Flux Parameters.*',bic_lines[i])):
            n_flow = int(bic_lines[i+2].split()[0])
            for j in xrange(n_flow):            
                l  = bic_lines[i+3 + j].split()
                if(len(flow)>0):
                    flow+='/ '            
            flow+= '%.1f slm' % float(l[1].replace('d','e') )
else:
    boundary = cfg.get('hydro_config.boundary_conditions')
    if(boundary):
        f = []
        for i in xrange(len(boundary)):
            t = cfg.get('hydro_config.boundary_conditions.[%i].bound_type' % i)
            if( t == "constant_mass_flow"):
                f.append(cfg.get('hydro_config.boundary_conditions.[%i].flux_value_slm.[0]' % i))
            if( t == "constant_pressure_bound"):
                pressure  = cfg.get('hydro_config.boundary_conditions.[%i].pressure.[0]' % i)
                temperature = cfg.get('hydro_config.boundary_conditions.[%i].temperature.[0]' % i)
        fs = []        
        for f1  in  f:
            fs.append('%.1f'% f1)
        flow = '/'.join(fs) +' slm'
                         
batch_pulse_count = float(cfg.get('pulse_config.batch_pulse_count'))
batch_void_count  = float(cfg.get('pulse_config.batch_void_count' ))
batch_length      = float(cfg.get('pulse_config.batch_length.[0]' ))
batch_legth_dim   = cfg.get('pulse_config.batch_length.[1]' )
PF_position = float(cfg.get('geometry.F1.[0]'))
IF_position = float(cfg.get('geometry.F2.[0]'))
duty_cycle = 'full'
if(batch_void_count > 0.0):
       duty_cycle = '%.0f/%.0f' % (batch_pulse_count,batch_pulse_count+batch_void_count)
rep_rate_dim = '1/'+ batch_legth_dim     
rep_rate     = (batch_void_count+batch_pulse_count)/batch_length
if(batch_legth_dim == 's'):
    rep_rate_dim = "kHz"
    rep_rate *= 1e-3
tex_file.write(r"""
\begin{table}[!h]
\centering
\parbox{0.5\textwidth}{
\centering
\caption{chamber conditions} 
\begin{tabularx}{0.5\textwidth}{l l }  
\toprule
pressure        & %(P).0f Pa\\
temperature     & %(T).0f K \\
flow            & %(F)s     \\
repetition rate & %(rep).2e %(rep_dim)s\\ 
duty cycle      & %(duty)s \\
PF position     & %(PF).1f \\
IF position     & %(IF).1f \\
\bottomrule
\end{tabularx}
}
\end{table}
    """ % {'P':pressure, 'T':temperature, 'F':flow
           ,'rep': rep_rate
           ,'rep_dim': rep_rate_dim
           ,'duty':duty_cycle
           ,'PF': PF_position
           ,'IF': IF_position})

tex_file.write(r"""
\begin{figure}[!h]
    \includegraphics[width=\textwidth]{images/boundary_conditions}
\end{figure}
""")        

#RZLINE parameters
ions_name      = cfg.get('pulse_config.ions_dist')
ions_cfg = pylibconfig.libconfigFile(args.path+ions_name)
print args.path+ions_name
#now let us make an energy ballance table
laser_energy     =  ions_cfg.get('energy_balance.laser_energy.[0]')
laser_reflection =  ions_cfg.get('energy_balance.laser_reflection')
laser_missing_droplet = ions_cfg.get('energy_balance.laser_missing_droplet')
euv_wide_band    =  ions_cfg.get('energy_balance.euv_wide_band')
euv_inband_4pi   =  ions_cfg.get('energy_balance.euv_inband_4pi')
euv_inband_2pi   =  ions_cfg.get('energy_balance.euv_inband_2pi')
plasma_energy    =  ions_cfg.get('energy_balance.plasma_energy')
fast_ion_full     = ions_cfg.get('energy_balance.fast_ion_full')
fast_ions_kinetic = ions_cfg.get('energy_balance.fast_ions_kinetic')
slow_ions_full    = ions_cfg.get('energy_balance.slow_ions_full')
atoms_in_droplet  = ions_cfg.get('energy_balance.atoms_in_droplet')
atoms_evaporated  = ions_cfg.get('energy_balance.atoms_evaporated')
momentum_forward  = ions_cfg.get('energy_balance.momentum_forward')
momentum_backward = ions_cfg.get('energy_balance.momentum_backward')
if(not os.path.isfile(results_path+'/mc_tracer_stat.txt')):       
    tex_file.write((r"""
\begin{table}[!h]
\caption{Energy balance (mJ)} 
\begin{tabularx}{\textwidth}{l D{.}{.}{1} c c c c c c} 
\toprule
                                        &\hbox{RZLINE}       &   \multicolumn{2}{c}{gas heating} & \multicolumn{2}{c}{mirror absorbtion}& \multicolumn{2}{c}{walls heating}\\
                                        &              &   "cold"   &   "hot"   & "cold"   &   "hot"    & "cold"   &   "hot"  \\
\midrule
incoming laser energy                   & %(laser).1f \\
Reflection of laser light               & %(lr).1f     &    ---     &   ---     &  ---     &   ---      &  ---     &    ---   \\                                     
Laser light missing droplet             & %(lm).1f     &    ---     &   ---     &  ---     &   ---      &  ---     &    ---   \\
Wide band EUV                           & %(WB).1f     &            &           &          &            &          &          \\
\hspace{0.2cm}EUV in band (in 4$\pi$)   & %(EUV4Pi).1f &    ---     &   ---     &  ---     &   ---      &  ---     &    ---   \\
\hspace{0.2cm}EUV in band (in 2$\pi$)   & %(EUV2Pi).1f &    ---     &   ---     &  ---     &   ---      &  ---     &    ---   \\
Fast ions and plasma                    & %(FIP).1f    &            &           &          &            &          &          \\
\hspace{0.2cm} ions kinetic energy      & %(ION_KIN).1f&            &           &          &            &          &          \\
\hspace{0.2cm} potential energy         & %(ION_POT).1f&            &           &          &            &          &          \\
\bottomrule
\end{tabularx}
\end{table}  
""" % {'laser' : laser_energy,
       'lr':laser_reflection*laser_energy,
       'lm':laser_missing_droplet*laser_energy,
       'WB':euv_wide_band*laser_energy,
       'EUV4Pi':euv_inband_4pi*laser_energy,
       'EUV2Pi':euv_inband_2pi*laser_energy,       
       'FIP':plasma_energy*laser_energy,
       'ION_KIN':(slow_ions_full+fast_ions_kinetic)*laser_energy,
       'ION_POT':(plasma_energy - fast_ions_kinetic - slow_ions_full)* laser_energy
       }))
else:
    try:
        data = np.genfromtxt(results_path+'/mc_tracer_stat.txt',skip_header=1,names=True)
        tex_file.write((r"""
\begin{table}[!h]
\caption{Energy balance (mJ)} 
\begin{tabularx}{\textwidth}{l D{.}{.}{1} c c c c c c c c} 
\toprule
                                        & \hbox{RZLINE}       &   \multicolumn{2}{c}{gas heating} & \multicolumn{2}{c}{mirror absorbtion}& \multicolumn{2}{c}{walls heating} & \multicolumn{2}{c}{inband escape\textcolor{red}{$^{a)}$}}\\
                                        &              &   "cold"   &   "hot"              & "cold"   &   "hot"    & "cold"   &   "hot"  & "cold"   &   "hot"  \\
\midrule                                                                                   
incoming laser energy                   & %(laser).1f \\                                        
Reflection of laser light               & %(lr).1f     &    ---     &   ---                &  ---     &   ---      &  ---     &    ---   &  ---     &    ---   \\                                     
Laser light missing droplet             & %(lm).1f     &    ---     &   ---                &  ---     &   ---      &  ---     &    ---   &  ---     &    ---   \\
Wide band EUV                           & %(WB).1f     &   %(WB1).1f&  %(WB2).1f           &%(WB3).1f &%(WB4).1f   &%(WB5).1f &%(WB6).1f &  %(WB7).1f   & %(WB8).1f  \\
\hspace{0.2cm}EUV in band (in 4$\pi$)   & %(EUV4Pi).1f &    ---       &   ---              &  ---     &   ---      &  ---     &    ---   &  ---         &    ---   \\
\hspace{0.2cm}EUV in band (in 2$\pi$)   & %(EUV2Pi).1f &    ---       &   ---              &  ---     &   ---      &  ---     &    ---   &  %(OUT1).1f  &   %(OUT2).1f   \\
Fast ions and plasma$^{b)}$               & %(FIP).1f    &  \\ 
\hspace{0.2cm} ions kinetic energy      & %(ION_KIN).1f&%(ION_KIN1).1f & %(ION_KIN2).1f     &          &            &          &          &              &          \\
\hspace{0.2cm} potential energy         & %(ION_POT).1f&%(ION_POT1).1f & %(ION_POT2).1f    &%(ION_POT3).1f&%(ION_POT4).1f&%(ION_POT5).1f&%(ION_POT6).1f  \\
\midrule
EUV to dissociation                     &              & %(DISS1).1f   & %(DISS2).1f \\
Sum gas heating                         &              & %(SUM1).1f    & %(SUM2).1f   \\
\bottomrule
\end{tabularx}
\textcolor{red}{a) The absolute accuracy of this value is poor due to spectrum simplification and other uncertainties.} However, the ratio between 'hot' and 'cold' state reflects changes in the transmission. \\
b)The ions potential energy is estimated from the RZLINE energy ballance as  $[\mbox{full energy of ions}] - [\mbox{ions kinetic, $E_{ion}$ > 1keV/ion}] - [\mbox{ions energy, $E_{ion}$ < 1keV/ion}]$, 
because, in most cases, fast ions have high charge, and slow ions are mostly Sn$^{+1}$ and Sn$^{+2}$. However, in some cases it is not accurate.
\end{table}  
""" % {'laser' : laser_energy,
       'lr':laser_reflection*laser_energy,
       'lm':laser_missing_droplet*laser_energy,
       'WB':euv_wide_band*laser_energy,
       'EUV4Pi':euv_inband_4pi*laser_energy,
       'EUV2Pi':euv_inband_2pi*laser_energy,       
       'FIP':plasma_energy*laser_energy,
       'ION_KIN':(slow_ions_full+fast_ions_kinetic)*laser_energy,
       'ION_POT':(plasma_energy - fast_ions_kinetic - slow_ions_full)* laser_energy,
       'WB1':data['radiation_wide_band_gas_heating'][0]*1e-4,
       'WB2':data['radiation_wide_band_gas_heating'][len(data)-1]*1e-4,           
       'WB3':data['radiation_wide_band_mirror_heating'][0]*1e-4, 
       'WB4':data['radiation_wide_band_mirror_heating'][len(data)-1]*1e-4,   
       'WB5':data['radiation_wide_band_wall_heating'][0]*1e-4, 
       'WB6':data['radiation_wide_band_wall_heating'][len(data)-1]*1e-4,    
       'WB7':data['radiation_wide_band_radiation_escape'][0]*1e-4,
       'WB8':data['radiation_wide_band_radiation_escape'][len(data)-1]*1e-4,           
       'ION_POT1':data['radiation_potential_energy_gas_heating'][0]*1e-4,
       'ION_POT2':data['radiation_potential_energy_gas_heating'][len(data)-1]*1e-4,   
       'ION_POT3':data['radiation_potential_energy_mirror_heating'][0]*1e-4, 
       'ION_POT4':data['radiation_potential_energy_mirror_heating'][len(data)-1]*1e-4,
       'ION_POT5':data['radiation_potential_energy_wall_heating'][0]*1e-4, 
       'ION_POT6':data['radiation_potential_energy_wall_heating'][len(data)-1]*1e-4,       
       'DISS1':data['total_h_energy'][0]*1e-4,
       'DISS2':data['total_h_energy'][len(data)-1]*1e-4,
       'ION_KIN1': data['ions_heating'][0]*1e-4,
       'ION_KIN2': data['ions_heating'][len(data)-1]*1e-4,
       'SUM1' :data['radiation_wide_band_gas_heating'][0]*1e-4 + data['radiation_potential_energy_gas_heating'][0]*1e-4 + data['ions_heating'][0]*1e-4 - data['total_h_energy'][0]*1e-4,
       'SUM2' :data['radiation_wide_band_gas_heating'][len(data)-1]*1e-4 + data['radiation_potential_energy_gas_heating'][len(data)-1]*1e-4 + data['ions_heating'][len(data)-1]*1e-4 - data['total_h_energy'][len(data)-1]*1e-4,   
       'OUT1' :data['radiation_wide_band_in_band_out'][0]*1e-4,
       'OUT2' :data['radiation_wide_band_in_band_out'][len(data)-1]*1e-4,
       }))
    except:
        pass      


#ions spectrum and radiation spectrum
#subprocess.check_output(["./plot_energy_spectrum.py","--output" , '%s/%s%s'%(images_dir,args.prefix,'ions_dist.png'), args.path+ions_name])
import plot_energy_spectrum
plot_energy_spectrum.plot_energy_spectrum(args.path+ions_name,'%s/%s%s'%(images_dir,args.prefix,'ions_dist.png')) 
ions_fig_name = 'images/'+args.prefix+'ions_dist.png'
radiation_name = cfg.get('pulse_config.radiation_dist')
import get_euv_sensor_value
get_euv_sensor_value.plot_anisotropy(args.path+radiation_name,'%s/%s%s'%(images_dir,args.prefix,'anisotropy.png'))
anisotropy_fig_name = 'images/' +args.prefix + 'anisotropy.png'
tex_file.write(r"""
\begin{figure}[h]
        \parbox{0.45\textwidth}{
    \centering
    \includegraphics[width=0.5\textwidth]{%s}  \\
    \caption{RZLINE calculated ion distribution as function of energy and polar angle. Laser hit the target from zero polar angle.}
    }
    \hfill
    \parbox{0.45\textwidth}{
    \centering
    \includegraphics[width=0.45\textwidth]{%s}  \\
    \caption{RZLINE calculated radiation anizotropy. Laser hit the target from zero polar angle. }
    }
\end{figure}
""" % (ions_fig_name,anisotropy_fig_name))    

import plotter

fig_mirror_reflectivity = plotter.plot_mirror_replectivity(args.path+'config.dat','MLM_reflectivity',images_dir,args.prefix)
fig_spectrum = plotter.plot_spectrum(args.path+ions_name,images_dir,args.prefix)

tex_file.write(r"""
\begin{figure}[!h]
    \parbox{0.45\textwidth}{
    \centering
    \includegraphics[width=0.5\textwidth]{images/%s}
    \caption{RZLINE calculated radiation spectrum.}
    }
    \hfill
    \parbox{0.45\textwidth}{
    \centering
    \includegraphics[width=0.45\textwidth]{images/%s}
    \caption{Mirror reflectivity curve.}
    }
\end{figure}
""" % (fig_spectrum,fig_mirror_reflectivity))  

    
if(MAKE_PARAVIEW_IMAGES):
    import make_report_paraview
    l = os.listdir(results_path)
    l = sorted(l,reverse=True)
    vtk_name = ''
    for a in l:
        if(re.findall('fig_.*.vtk',a) and a[0]!='.'):
            vtk_name = a        
            break
    if(len(vtk_name) < 1):
        print "no vtk!"
    else:
        #import reading_boundary_data
        #boundary_image = reading_boundary_data.get_boundary_conditions(results_path + '/' + vtk_name,images_dir,args.prefix)
        if(args.state_file == ''):
	       L = os.listdir('.')
               states      = []
               states_pres = []
               for i in L:
                   if( re.findall("paraview_two_horisontal\.\d+Pa\.pvsm", i) ):
                         states.append(i)
                         states_pres.append(float(re.findall("paraview_two_horisontal\.(\d+)Pa\.pvsm", i)[0]))
               states_pres=np.array(states_pres) 
               def find_nearest(array,value):
                   idx = (np.abs(array-value)).argmin()
                   return idx
               args.state_file = states[find_nearest(states_pres,pressure)]
        paraview_fig = make_report_paraview.make_images(path=results_path + '/' + vtk_name,state_file=args.state_file,prefix=args.prefix,output_dir=images_dir)

        tex_file.write(r"""\begin{figure}
            \centering
            """)
        new_line_flag = False
        for fig  in paraview_fig['fig_names']:
            #for vertical images
            #tex_file.write(r"""
                #\parbox{0.45\textwidth}{
                    #\includegraphics[width=0.5\textwidth]{images/%s}
                #}""" % fig)
            #if(not new_line_flag):
                #tex_file.write(r'\hfill')
                #new_line_flag = True
            #else:
                #tex_file.write(r'\\')
                #new_line_flag = False       
            #horisontal images    
            tex_file.write(r"""\includegraphics[width=0.7\textwidth]{images/%s} \\
                """ % fig)    
        file_time =  dropline_and_rays_out.get_time_field(paraview_fig['vtk_name'])
        tex_file.write(r"""\caption{Gas dynamics snapshot at %.1f ms.}""" % file_time)        
        tex_file.write(r"""\end{figure}""")        
        



#contamination plots
#out = subprocess.check_output(["./plot_hydro_stat.py","--output_dir" , images_dir,"--prefix",args.prefix, results_path + '/HydroSurfaceStat.txt'])
if(os.path.isfile(results_path+'/HydroSurfaceStat.txt')):
    import plot_hydro_stat
    contamination_fignames = plot_hydro_stat.make_contamination_plots(results_path + '/HydroSurfaceStat.txt',images_dir,args.prefix)
    tex_file.write(r"""
    \begin{figure}
        \parbox{0.45\textwidth}{
        \centering
        \includegraphics[width=0.5\textwidth]{images/%s}   \\ 
        %s
        }
        \hfill
        \parbox{0.45\textwidth}{
        \centering
        \includegraphics[width=0.5\textwidth]{images/%s}  \\ 
        %s
        }
        \caption{Contamination rate due to thermalised tin flux to mirror (i.e. $f = 1/4 n V$). Conversion factor is 1/(cm$^2$ $\cdot$ s) = $3600.0/3.3e15$ nm/hour.     }
    \end{figure}
    """ % (contamination_fignames[0],check_for_source(contamination_fignames[0]),contamination_fignames[1],  check_for_source(contamination_fignames[1]) ))

import make_flux_edf

if(os.path.isfile(results_path+'/surface_hit_stat.txt')):
    fig_hit_stat, pulse_number = make_flux_edf.make_angular_plot(results_path+'/surface_hit_stat.txt',args.primary_focus,images_dir,args.prefix)
    if(pulse_number < 0):
        pn = ''
    else:
        pn = 'Pulse number %i.' % pulse_number
    lrep_rate = batch_pulse_count/batch_length
    if(batch_legth_dim != 's'):
        lrep_rate = 0.0
    fig_hit_pulse_integral = make_flux_edf.make_radial_plot(results_path+'/surface_hit_stat.txt',results_path+'/HydroSurfaceStat.txt',images_dir,args.prefix,rep_rate=lrep_rate)
    tex_file.write(r"""
\begin{figure}
    \parbox{0.45\textwidth}{
    \centering
    \includegraphics[width=0.5\textwidth]{images/%s}  \\  
    %s
    }
    \hfill    
    \parbox{0.45\textwidth}{
    \centering
    \includegraphics[width=0.5\textwidth]{images/%s} \\   
    %s
    }
    \caption{Non-stopped ions energy distribution function and energy integrated ion flux. %s }
\end{figure}
""" % (fig_hit_stat,check_for_source(fig_hit_stat),fig_hit_pulse_integral,check_for_source(fig_hit_pulse_integral),pn))
#\parbox{0.45\textwidth}{
    #\centering
    #\includegraphics[width=0.5\textwidth]{%s}    
    #}
    #\caption{.
    #}
#paraview images

#density integral plots
#for angle in (np.array([15.0,30.0,45.0,60.0,75.0,90.0])/180.0*3.14):
   #subprocess.check_output(["./dropline_and_rays_out.py","--output_dir" , images_dir,"--prefix",args.prefix,'--IF','160.0','--PF','19.0','--angle','%.2f'% angle, results_path + '/'])
   #subprocess.check_output(["./dropline_and_rays_out.py","--output_dir" , images_dir,"--prefix",args.prefix,'--IF','160.0','--PF','19.0',"--trace_to_IF",'--angle','%.2f'% angle, results_path + '/'])

#MAKE_DENSITY_INTEGRALS = False
if(MAKE_DENSITY_INTEGRALS):
    density_integrals = []
    for angle in (np.array([15.0,30.0,45.0,60.0,75.0,90.0])/180.0*np.pi):
        fig_name = dropline_and_rays_out.make_density_integral_pictures(results_path + '/',PF_position,IF_position,False,angle,images_dir,args.prefix)
        density_integrals.append(fig_name)

    tex_file.write(r"""\begin{figure}""")
    new_line_flag = False
    for fig  in density_integrals:
        #source = ''
        #if(os.path.isfile(images_dir + '/' + fig +'.csv')):            
            #source = r'\textattachfile{images/%s}{\textcolor{blue}{\detokenize{%s}}}' % (fig+'.csv',fig+'.csv')          
            
        tex_file.write(r"""
            \parbox{0.45\textwidth}{
                \begin{center}
                \includegraphics[width=0.5\textwidth]{images/%s} \\
                %s
                \end{center}
            }""" % (fig,check_for_source(fig)))                
        if(not new_line_flag):
            tex_file.write(r'\hfill')
            new_line_flag = True
        else:
            tex_file.write(r'\\')
            new_line_flag = False        
    #file_time =  dropline_and_rays_out.get_time_field(paraview_fig['vtk_name'])
    tex_file.write(r"""\caption{Density integrals from PF to mirror}""")        
    tex_file.write(r"""\end{figure}""")

    density_integrals = []
    for angle in (np.array([15.0,30.0,45.0,60.0,75.0])/180.0*np.pi):
        fig_name = dropline_and_rays_out.make_density_integral_pictures(results_path + '/',PF_position,IF_position,True,angle,images_dir,args.prefix)
        density_integrals.append(fig_name)

    tex_file.write(r"""\begin{figure}""")
    new_line_flag = False
    for fig  in density_integrals:        
        tex_file.write(r"""
            \parbox{0.45\textwidth}{
                \begin{center}
                \includegraphics[width=0.5\textwidth]{images/%s} \\
                %s
                \end{center}
            }""" % (fig,check_for_source(fig)))
        
        if(not new_line_flag):
            tex_file.write(r'\hfill')
            new_line_flag = True
        else:
            tex_file.write(r'\\')
            new_line_flag = False        
    #file_time =  dropline_and_rays_out.get_time_field(paraview_fig['vtk_name'])
    tex_file.write(r"""\caption{Density integrals from PF to IF}""")        
    tex_file.write(r"""\end{figure}""")

import energy_fluxes
if(os.path.isfile(results_path+'/pulses_list.txt')):
    fig_source = energy_fluxes.get_source_energy_input(results_path+'/pulses_list.txt',output_dir=images_dir,prefix=args.prefix)
    
    tex_file.write(r"""
    \begin{figure}[h]
        \centering
        \includegraphics[width=0.8\textwidth]{images/%s}  \\
        %s
        \caption{Energy and mass input from plasma to gas per pulse.}          
    \end{figure}    
    """ % (fig_source, check_for_source(fig_source)))  

fig_energy_flux, energy_flux_averages = energy_fluxes.make_energy_flux_plot(results_path+'/Balance.txt',output_dir=images_dir,prefix=args.prefix)

fig_mass_flux,mass_flux_averages = energy_fluxes.make_mass_flux_plot(results_path+'/Balance.txt',output_dir=images_dir,prefix=args.prefix)
    
fig_integral_mass, fig_integral_energy = energy_fluxes.make_chamber_integrals_plot(results_path+'/Balance.txt',output_dir=images_dir,prefix=args.prefix)

tex_file.write(r"""
\begin{table}[h]
\centering
\parbox{0.5\textwidth}{
\caption{Energy fluxes in simulation (kW) }
\begin{tabular*}{0.5\textwidth}{@{\extracolsep{\fill} }l c}
\toprule
""")
flux_sum = 0.0
for line in  energy_flux_averages:    
    tex_file.write (r" %s & %.2e \\" % (line[0],line[1]))
    tex_file.write ('\n')
    flux_sum+= line[1]
tex_file.write(r"-- fluxes sum (source) & %.2e \\" % (-flux_sum))    
tex_file.write(r"""
\bottomrule
\end{tabular*}
Energy fluxes averaged over last 5 ms.
}
\end{table}
""")

tex_file.write(r"""
\begin{figure}[h]
     \centering
     \includegraphics[width=0.8\textwidth]{images/%s}  \\
     %s
     \caption{Energy fluxes in simulation. }          
\end{figure}    
""" % (fig_energy_flux, check_for_source(fig_energy_flux)))   

tex_file.write(r"""
\begin{figure}       
    \begin{center}
    \includegraphics[width=0.5\textwidth]{images/%s} \\
    %s
    \caption{Integral energy. }
    \end{center}
\end{figure}    
""" % (fig_integral_energy,check_for_source(fig_integral_energy)))    


tex_file.write(r"""
\begin{table}[h]
\centering
\parbox{0.5\textwidth}{
\caption{Mass fluxes in simulation (g/s)}
\begin{tabular*}{0.5\textwidth}{@{\extracolsep{\fill} }l c}
\toprule
""")
for line in  mass_flux_averages:    
    tex_file.write (r" %s & %.2e \\" % (line[0],line[1]))
    tex_file.write ('\n')    
tex_file.write(r"""
\bottomrule
\end{tabular*}
Mass fluxes averaged over last 5 ms.
}
\end{table}
""")

tex_file.write(r"""
\begin{figure}[h]
     \parbox{0.49\textwidth}{
        \centering
        \includegraphics[width=0.5\textwidth]{images/%s}  \\
        %s
        \caption{Mass fluxes in simulation. }          
     }
     \hfill
     \parbox{0.49\textwidth}{
        \centering
        \includegraphics[width=0.5\textwidth]{images/%s}  \\
        %s
        \caption{Integral mass. }          
     }
\end{figure}    
""" % (fig_mass_flux, check_for_source(fig_mass_flux),fig_integral_mass,check_for_source(fig_integral_mass)))   




tex_file.write(r'\end{document}')
exit()    

