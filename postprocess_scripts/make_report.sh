#!/bin/bash

export LD_LIBRARY_PATH='/usr/local/lib/paraview-4.3/:/usr/local/lib/paraview-4.3/site-packages/vtk/'
export PYTHONPATH="$PYTHONPATH:/usr/local/lib/paraview-4.3/site-packages/:/usr/local/lib/paraview-4.3/site-packages/vtk/"
#$PYTHONPATH="$PYTHONPATH:/home/astakhov/2DPGI/scripts/"
module load  mpi/openmpi-x86_64
scripts_path='/home/astakhov/2DPGI/scripts/'
path=$1
$scripts_path/make_report.py $path
cd $path/report
fname=`ls ../ResultData/ | grep vtk | head -1`
$scripts_path/reading_boundary_data.py ../ResultData/$fname 
mv boundary_conditions.pdf images/
xelatex -halt-on-error report*.tex
$scripts_path/report_copy_vtk.py ../ResultData
cd -



