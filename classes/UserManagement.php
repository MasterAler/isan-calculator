<?php

require_once("./libraries/ps_pagination.php");

/**
 * Class registration
 * handles the user registration
 */
class UserManagement
{
    /**
     * @var object $db_connection The database connection
     */
    private $db_connection = null;
    /**
     * @var array $errors Collection of error messages
     */
    public $errors = array();
    /**
     * @var array $messages Collection of success / neutral messages
     */
    public $messages = array();

    public function __construct()
    {
        // create a database connection
        $this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        // change character set to utf8 and check it
        if (!$this->db_connection->set_charset("utf8")) {
            $this->errors[] = $this->db_connection->error;
        }

        if (isset($_POST["register"])) {
            $this->registerNewUser();
        }

        if (isset($_POST["change_password"])) {
            $this->changeUserPassword();
        }
    }

    public function userList()
    {
        if (!$this->db_connection->connect_errno) {

            $sql = "SELECT user_id, user_name, user_email FROM calc_users;";
            $query_users_list = $this->db_connection->query($sql);
            $result = $query_users_list->fetch_all(MYSQLI_ASSOC);
            $query_users_list->close();
            return $result;

        } else {
            $this->errors[] = "Sorry, no database connection.";
            return array();
        }
    }

    public function userPaginator()
    {
        $conn = mysql_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        mysql_select_db('db_users', $conn);
        $sql = "SELECT user_id, user_name, user_email FROM calc_users";
        $pager = new PS_Pagination($conn, $sql, 10, 5, "");
        return $pager;
    }

    /**
     * handles the entire registration process. checks all error possibilities
     * and creates a new user in the database if everything is fine
     */
    private function registerNewUser()
    {
        if (empty($_POST['user_name'])) {
            $this->errors[] = "Empty Username";
        } elseif (empty($_POST['user_password_new']) || empty($_POST['user_password_repeat'])) {
            $this->errors[] = "Empty Password";
        } elseif ($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
            $this->errors[] = "Password and password repeat are not the same";
        } elseif (strlen($_POST['user_password_new']) < 6) {
            $this->errors[] = "Password has a minimum length of 6 characters";
        } elseif (strlen($_POST['user_name']) > 64 || strlen($_POST['user_name']) < 2) {
            $this->errors[] = "Username cannot be shorter than 2 or longer than 64 characters";
        } elseif (!preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])) {
            $this->errors[] = "Username does not fit the name scheme: only a-Z and numbers are allowed, 2 to 64 characters";
        } elseif (empty($_POST['user_email'])) {
            $this->errors[] = "Email cannot be empty";
        } elseif (strlen($_POST['user_email']) > 64) {
            $this->errors[] = "Email cannot be longer than 64 characters";
        } elseif (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = "Your email address is not in a valid email format";
        } elseif (!empty($_POST['user_name'])
            && strlen($_POST['user_name']) <= 64
            && strlen($_POST['user_name']) >= 2
            && preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])
            && !empty($_POST['user_email'])
            && strlen($_POST['user_email']) <= 64
            && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
            && !empty($_POST['user_password_new'])
            && !empty($_POST['user_password_repeat'])
            && ($_POST['user_password_new'] === $_POST['user_password_repeat'])
        ) {

            // if no connection errors (= working database connection)
            if (!$this->db_connection->connect_errno) {

                // escaping, additionally removing everything that could be (html/javascript-) code
                $user_name = $this->db_connection->real_escape_string(strip_tags($_POST['user_name'], ENT_QUOTES));
                $user_email = $this->db_connection->real_escape_string(strip_tags($_POST['user_email'], ENT_QUOTES));

                $user_password = $_POST['user_password_new'];

                // crypt the user's password with PHP 5.5's password_hash() function, results in a 60 character
                // hash string. the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using
                // PHP 5.3/5.4, by the password hashing compatibility library
                $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT);

                // check if user or email address already exists
                $sql = "SELECT * FROM calc_users WHERE user_name = '" . $user_name . "' OR user_email = '" . $user_email . "';";
                $query_check_user_name = $this->db_connection->query($sql);

                if ($query_check_user_name->num_rows == 1) {
                    $this->errors[] = "Sorry, that username / email address is already taken.";
                } else {
                    // write new user's data into database
                    $sql = "INSERT INTO calc_users (user_name, user_password_hash, user_email)
                            VALUES('" . $user_name . "', '" . $user_password_hash . "', '" . $user_email . "');";
                    $query_new_user_insert = $this->db_connection->query($sql);

                    // if user has been added successfully
                    if ($query_new_user_insert) {
                        $this->messages[] = "Your account has been created successfully. You can now log in.";
                    } else {
                        $this->errors[] = "Sorry, your registration failed. Please go back and try again.";
                    }
                }
                $query_check_user_name->close();

            } else {
                $this->errors[] = "Sorry, no database connection.";
            }
        } else {
            $this->errors[] = "An unknown error occurred.";
        }
    }

    private function changeUserPassword()
    {
        if (empty($_POST['new_password'])) {
            $this->errors[] = "Empty new password";
        } elseif ($_POST['new_password'] !== $_POST['new_password_repeat']) {
            $this->errors[] = "Password and password repeat are not the same";
        } elseif (strlen($_POST['new_password']) < 6) {
            $this->errors[] = "Password has a minimum length of 6 characters";
        } elseif (empty($_POST['old_password'])) {
            $this->errors[] = "Empty old password";
        } else {

            if (!$this->db_connection->connect_errno) {

                $user_name = $_SESSION["user_name"];
                $sql = "SELECT user_id, user_email, user_password_hash
                        FROM calc_users
                        WHERE user_name = '" . $user_name . "' OR user_email = '" . $user_name . "';";
                $result_of_login_check = $this->db_connection->query($sql);

                if ($result_of_login_check->num_rows == 1) {

                    $result_row = $result_of_login_check->fetch_object();

                    if (password_verify($_POST['old_password'], $result_row->user_password_hash)) {

                        $user_password_hash = password_hash($_POST['new_password'], PASSWORD_DEFAULT);
                        $sql = "UPDATE calc_users
                         SET user_password_hash = '" . $user_password_hash . "' WHERE user_name = '" . $user_name . "';";
                        $query_pwd_update = $this->db_connection->query($sql);

                        if ($query_pwd_update) {
                            $this->messages[] = "Password successfully changed.";
                        } else {
                            $this->errors[] = "Sorry, something went wrong, try again later.";
                        }

                    } else {
                        $this->errors[] = "Wrong password. Try again.";
                    }

                } else {
                    $this->errors[] = "You're a fucking ghost.";
                }
                $result_of_login_check->close();

            } else {
                $this->errors[] = "Sorry, no database connection.";
            }
        }
    }
}
