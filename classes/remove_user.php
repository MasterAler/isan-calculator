<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/config/db.php");
require_once("Login.php");

function delete_path($path)
{
    if (is_dir($path))
    {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file)
            delete_path(realpath($path) . '/' . $file);

        return rmdir($path);
    }
    else if (is_file($path))
        return unlink($path);

    return false;
}


$login = new Login();
if (!$login->isUserLoggedIn() || ($_SESSION["user_name"] !== Login::ADMIN_USERNAME))
    die("FUCK YOU");

if(empty($_POST["id"])) {
    die("no valid id");
} else {

    $db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    // change character set to utf8 and check it
    if (!$db_connection->set_charset("utf8")) {
        die($db_connection->error);
    }

    if (!$db_connection->connect_errno) {

        $sql = "DELETE FROM calc_users WHERE user_id = " . $_POST["id"] . ";";
        $query_remove_user = $db_connection->query($sql);

        if ($query_remove_user) {
            echo "user deleted";
        } else {
            echo "user not deleted somehow";
        }

    } else {
        die("Sorry, no database connection.");
    }
}
