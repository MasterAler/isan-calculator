<?php
//header('Access-Control-Allow-Origin: *'); 

require_once("../config/db.php");
require_once("Login.php");
require_once("../libraries/password_compatibility_library.php");

$login = new Login();
if (!$login->isUserLoggedIn() || ($_SESSION["user_name"] !== Login::ADMIN_USERNAME))
    die("FUCK YOU");

$db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// change character set to utf8 and check it
if (!$db_connection->set_charset("utf8")) {
    die($db_connection->error);
}

if (!$db_connection->connect_errno) {

    if (empty($_POST['user_name'])) {
       die("Empty Username");
    } elseif (empty($_POST['user_password_new'])) {
       die("Empty Password");
    } elseif ($_POST['user_password_new'] !== $_POST['user_password_repeat']) {
       die("Password and password repeat are not the same");
    } elseif (strlen($_POST['user_password_new']) < 6) {
       die("Password has a minimum length of 6 characters");
    } elseif (strlen($_POST['user_name']) > 64 || strlen($_POST['user_name']) < 2) {
       die("Username cannot be shorter than 2 or longer than 64 characters");
    } elseif (!preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])) {
       die("Username does not fit the name scheme: only a-Z and numbers are allowed, 2 to 64 characters");
    } elseif (empty($_POST['user_email'])) {
       die("Email cannot be empty");
    } elseif (strlen($_POST['user_email']) > 64) {
       die("Email cannot be longer than 64 characters");
    } elseif (!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
       die("Your email address is not in a valid email format");
    } elseif (!empty($_POST['user_name'])
        && strlen($_POST['user_name']) <= 64
        && strlen($_POST['user_name']) >= 2
        && preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])
        && !empty($_POST['user_email'])
        && strlen($_POST['user_email']) <= 64
        && filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)
        && !empty($_POST['user_password_new'])
        && ($_POST['user_password_new'] === $_POST['user_password_repeat'])
    ) {

        // escaping, additionally removing everything that could be (html/javascript-) code
        $user_name = $db_connection->real_escape_string(strip_tags($_POST['user_name'], ENT_QUOTES));
        $user_email = $db_connection->real_escape_string(strip_tags($_POST['user_email'], ENT_QUOTES));

        $user_password = $_POST['user_password_new'];

        // crypt the user's password with PHP 5.5's password_hash() function, results in a 60 character
        // hash string. the PASSWORD_DEFAULT constant is defined by the PHP 5.5, or if you are using
        // PHP 5.3/5.4, by the password hashing compatibility library
        $user_password_hash = password_hash($user_password, PASSWORD_DEFAULT);

        // check if user or email address already exists
        $sql = "SELECT * FROM calc_users WHERE user_name = '" . $user_name . "' OR user_email = '" . $user_email . "';";
        $query_check_user_name = $db_connection->query($sql);

        if ($query_check_user_name->num_rows == 1) {
            die("Sorry, that username / email address is already taken.");
        } else {
            // write new user's data into database
            $sql = "INSERT INTO calc_users (user_name, user_password_hash, user_email)
                            VALUES('" . $user_name . "', '" . $user_password_hash . "', '" . $user_email . "');";
            $query_new_user_insert = $db_connection->query($sql);

            // if user has been added successfully
            if ($query_new_user_insert) {
                $dir = '/var/www/html/archive/' . $user_name;
                if (!is_dir($dir)){
                    if (mkdir($dir))
                        echo 200;
                    else
                        die($dir);
                }
            } else {
                die("Sorry, your registration failed. Please go back and try again.");
            }
        }
        $query_check_user_name->close();

    } else {
       die("An unknown error occurred.");
    }
    
} else {
    die("Sorry, no database connection.");
}

