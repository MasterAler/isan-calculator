<?php
/**
 * Created by PhpStorm.
 * User: Aler
 * Date: 21.10.14
 * Time: 4:15
 */
 
 /**
 * Indents a flat JSON string to make it more human-readable.
 *
 * @param string $json The original JSON string to process.
 *
 * @return string Indented version of the original JSON string.
 */
function indent($json) {

    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '  ';
    $newLine     = "\n";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

        // Grab the next character in the string.
        $char = substr($json, $i, 1);

        // Are we inside a quoted string?
        if ($char == '"' && $prevChar != '\\') {
            $outOfQuotes = !$outOfQuotes;

        // If this character is the end of an element,
        // output a new line and indent the next line.
        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
            $result .= $newLine;
            $pos --;
            for ($j=0; $j<$pos; $j++) {
                $result .= $indentStr;
            }
        }

        // Add the character to the result string.
        $result .= $char;

        // If the last character was the beginning of an element,
        // output a new line and indent the next line.
        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
            $result .= $newLine;
            if ($char == '{' || $char == '[') {
                $pos ++;
            }

            for ($j = 0; $j < $pos; $j++) {
                $result .= $indentStr;
            }
        }

        $prevChar = $char;
    }

    return $result;
}

$filename= $_SERVER["DOCUMENT_ROOT"] . "/calc/params/" . $_POST["config_data"];
$fsize = filesize($filename);
$dest_name = "New_Config.json";

$params = $_POST;
unset($params["config_data"]);
$obj = json_decode(file_get_contents($filename));
$obj->{"export"} = $params;
$result = indent(json_encode($obj));
$fsize = strlen($result);

header('HTTP/1.1 200 OK');
header("Content-Length: $fsize");
header("Content-Description: file transfer");
header("Content-transfer-encoding: binary");
header('Content-Disposition: attachment; filename="' . $dest_name . '"');

print $result;
exit;
