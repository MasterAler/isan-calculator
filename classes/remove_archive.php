<?php
require_once("CalcHistory.php");

if(empty($_POST["id"])) {
    die("no valid id");
} else if (empty($_POST["user_name"])) {
    die("no valid user");
} else {
    $ch = new CalcHistory();
    echo (int)$ch->removeItem($_POST["user_name"], $_POST["id"]);
}
