<?php

error_reporting(E_ALL);
$filename = $_POST["filename"];
$user = $_POST["user"];
unset($_POST["filename"]);
unset($_POST["user"]);

$data = array('filename'=>$filename, 'vars'=>$_POST, 'user'=>$user);

$context = new ZMQContext();

//  Socket to talk to server
$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
$requester->connect("tcp://127.0.0.1:43000");
//$requester->connect("tcp://91.235.244.101:43000");

$requester->send(json_encode(array(
            'command' => 'launch', 
            'args' => array('params'=> $data) 
            )));

echo 0;
//echo $requester->recv();
