<?php

class ConfigLoader
{
    private $json_dir = null;
    private $dat_dir = null;
    private $archive_dir = null;
    private $configs = null;
	private $script_dir = null;
    private $dats = null;
	private $scripts = null;

    public function __construct()
    {
        $this->json_dir = $_SERVER["DOCUMENT_ROOT"] . "/json_configs/";
        $this->dat_dir = $_SERVER["DOCUMENT_ROOT"] . "/dat_configs/";
        $this->archive_dir = $_SERVER["DOCUMENT_ROOT"] . "/archive";
		$this->script_dir = $_SERVER["DOCUMENT_ROOT"] . "/postprocess_scripts/";

        $dir = opendir($this->json_dir);
        while ($jfile = readdir($dir)) 
        {
            $filename = $this->json_dir . $jfile;
            if (is_file($filename))
                $this->configs[$jfile] = file_get_contents($filename);
        } 
        closedir($dir);

        $dir = opendir($this->dat_dir);
        while ($jfile = readdir($dir))
        {
            if (is_file($this->dat_dir . $jfile))
                $this->dats[] = $jfile;
        }
        closedir($dir);
		
		$dir = opendir($this->script_dir);
        while ($jfile = readdir($dir))
        {
            if (is_file($this->script_dir . $jfile))
                $this->scripts[] = $jfile;
        }
        closedir($dir);
    }

    public function getConfigs()
    {
        return $this->configs;
    }

    public function getDatFileNames()
    {
        return $this->dats;
    }

    public function isWaiting()
    {
        return file_exists($this->archive_dir . '/' . $_SESSION['user_name'] . '/' . "LOCK");
    }

    public function getConfigsJson()
    {
        $result = array();
        foreach(array_keys($this->configs) as $conf)
        {
            $result[$conf] = json_decode($this->configs[$conf]);
        }

        return json_encode($result);
    }
	
	public function getPostProcessScripts()
	{
		return $this->scripts;
	}

    function removeItem($item)
    {
        return unlink($this->dat_dir . $item) and unlink($this->json_dir . basename($item, ".dat"));
    }

} 