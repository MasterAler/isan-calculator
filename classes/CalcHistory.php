<?php

class CalcHistory
{
    public $errors = array();
    public $messages = array();

    private static function delete_path($path)
    {
        if (is_dir($path))
        {
            $files = array_diff(scandir($path), array('.', '..'));

            foreach ($files as $file)
                CalcHistory::delete_path(realpath($path) . '/' . $file);

            return rmdir($path);
        }
        else if (is_file($path))
            return unlink($path);

        return false;
    }

    public function archivePath()
    {
        return "./archive/" . $_SESSION["user_name"] . '/';
    }

    public function removeItem($user, $filename)
    {
        return CalcHistory::delete_path("/var/www/html/archive/" . $user . '/' .  basename($filename, ".tar.gz"))  and 
            unlink("/var/www/html/archive/" . $user . '/' . $filename);
    }

    public function fetchCalcHistory()
    {
        $result = array();
        // $dir = "/var/www/html/calc/archive";
        $dir = "./archive/" . $_SESSION["user_name"];
        $files = scandir($dir);    //сканируем (получаем массив файлов)
        array_shift($files); // удаляем из массива '.'
        array_shift($files); // удаляем из массива '..'
        for($i=0; $i<sizeof($files); $i++)
        {
         $format = array_pop(explode(".",$files[$i]));             
         if( $format == 'gz')
            $result[] = $files[$i];
        }
         
        return $result;
    }

    public function fetchOldArchives()
    {
        $result = array();
        $dir = "./2DPGI";
        $files = scandir($dir);    //сканируем (получаем массив файлов)
        array_shift($files); // удаляем из массива '.'
        array_shift($files); // удаляем из массива '..'
        for($i=0; $i<sizeof($files); $i++)
        {
            $format = array_pop(explode(".",$files[$i]));
            $result[] = $files[$i];
        }

        return $result;
    }
} 